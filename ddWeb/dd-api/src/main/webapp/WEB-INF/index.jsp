<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="x-ua-compatible" content="IE=edge">
<title>Swagger UI</title>
<c:url value="/resources/images/favicon-32x32.png"></c:url>


<link rel="icon" type="image/png"
	href="<c:url value="/resources/images/favicon-32x32.png"></c:url>"
	sizes="32x32" />


<link rel="icon" type="image/png"
	href='<c:url value="/resources/images/favicon-16x16.png"></c:url>'
	sizes="16x16" />


<link href='<c:url value="/resources/css/typography.css"></c:url>'
	media='screen' rel='stylesheet' type='text/css' />



<link href='<c:url value="/resources/css/reset.css"></c:url>'
	media='screen' rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/css/screen.css"></c:url>'
	media='screen' rel='stylesheet' type='text/css' />
<c:url value="/resources/css/screen.css"></c:url>
<link href='<c:url value="/resources/css/reset.css"></c:url>'
	media='print' rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/css/print.css"></c:url>'
	media='print' rel='stylesheet' type='text/css' />

<c:url value="/resources/lib/object-assign-pollyfill.js"></c:url>

<script
	src='<c:url value="/resources/lib/object-assign-pollyfill.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/jquery-1.8.0.min.js"></c:url>'
	type='text/javascript'></script>
<script
	src='<c:url value="/resources/lib/jquery.slideto.min.js"></c:url>'
	type='text/javascript'></script>
<script
	src='<c:url value="/resources/lib/jquery.wiggle.min.js"></c:url>'
	type='text/javascript'></script>
<script
	src='<c:url value="/resources/lib/jquery.ba-bbq.min.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/handlebars-4.0.5.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/lodash.min.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/backbone-min.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/swagger-ui.js"></c:url>'
	type='text/javascript'></script>
<script
	src='<c:url value="/resources/lib/highlight.9.1.0.pack.js"></c:url>'
	type='text/javascript'></script>
<script
	src='<c:url value="/resources/lib/highlight.9.1.0.pack_extended.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/jsoneditor.min.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/marked.js"></c:url>'
	type='text/javascript'></script>
<script src='<c:url value="/resources/lib/swagger-oauth.js"></c:url>'
	type='text/javascript'></script>

<!-- Some basic translations -->
<!-- <script src='lang/translator.js' type='text/javascript'></script> -->
<!-- <script src='lang/ru.js' type='text/javascript'></script> -->
<!-- <script src='lang/en.js' type='text/javascript'></script> -->

<script type="text/javascript">
	$(function() {
		var url = window.location.search.match(/url=([^&]+)/);
		var apiUrl = window.location.protocol + "//" + window.location.host;
		if (window.location.pathname.indexOf('/api') > 0) {
			apiUrl += window.location.pathname.substring(0,
					window.location.pathname.indexOf('/api'))
		}
		apiUrl += "/api-docs";

		hljs.configure({
			highlightSizeThreshold : 5000
		});

		// Pre load translate...
		if (window.SwaggerTranslator) {
			window.SwaggerTranslator.translate();
		}
		window.swaggerUi = new SwaggerUi(
				{
					url : url,
					dom_id : "swagger-ui-container",
					supportedSubmitMethods : [ 'get', 'post', 'put', 'delete',
							'patch' ],
					onComplete : function(swaggerApi, swaggerUi) {
						if (typeof initOAuth == "function") {
							initOAuth({
								clientId : "your-client-id",
								clientSecret : "your-client-secret-if-required",
								realm : "your-realms",
								appName : "your-app-name",
								scopeSeparator : " ",
								additionalQueryStringParams : {}
							});
						}

						if (window.SwaggerTranslator) {
							window.SwaggerTranslator.translate();
						}
					},
					onFailure : function(data) {
						log("Unable to Load SwaggerUI");
					},
					docExpansion : "none",
					jsonEditor : false,
					defaultModelRendering : 'schema',
					showRequestHeaders : false,
					showOperationIds : false
				});

		window.swaggerUi.load();

		function log() {
			if ('console' in window) {
				console.log.apply(console, arguments);
			}
		}
	});
</script>
</head>

<body class="swagger-section">
	<div id='header'>
		<div class="swagger-ui-wrap">
			<a id="logo" href="http://swagger.io"><img class="logo__img"
				alt="swagger" height="30" width="30" src="images/logo_small.png" /><span
				class="logo__title">swagger</span></a>
			<form id='api_selector'>
				<div class='input'>
					<input placeholder="http://example.com/api" id="input_baseUrl"
						name="baseUrl" type="text" />
				</div>
				<div id='auth_container'></div>
				<div class='input'>
					<a id="explore" class="header__btn" href="#" data-sw-translate>Explore</a>
				</div>
			</form>
		</div>
	</div>

	<div id="message-bar" class="swagger-ui-wrap" data-sw-translate>&nbsp;</div>
	<div id="swagger-ui-container" class="swagger-ui-wrap"></div>
</body>
</html>
