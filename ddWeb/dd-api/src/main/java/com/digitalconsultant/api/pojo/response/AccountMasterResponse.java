package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.AccountMasterDto;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public class AccountMasterResponse extends Response {

    private AccountMasterDto accountDetails;

    public AccountMasterDto getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(AccountMasterDto accountDetails) {
        this.accountDetails = accountDetails;
    }
}
