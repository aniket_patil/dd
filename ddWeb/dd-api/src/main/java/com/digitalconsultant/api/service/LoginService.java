package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.ForgotPasswordRequest;
import com.digitalconsultant.api.pojo.request.LoginRequest;
import com.digitalconsultant.api.pojo.request.RegistrationRequest;
import com.digitalconsultant.api.pojo.response.LoginResponse;
import com.digitalconsultant.api.pojo.response.Response;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface LoginService {

    LoginResponse login(ApiRequestDto<LoginRequest> apiRequestDto) throws CustomException;

    Response registration(ApiRequestDto<RegistrationRequest> apiRequestDto) throws CustomException;

    Response forgotPassword(ApiRequestDto<ForgotPasswordRequest> apiRequestDto) throws Exception;

    Response logout(String sessionToken);


}
