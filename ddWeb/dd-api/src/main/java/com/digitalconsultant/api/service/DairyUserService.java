package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDairyUserRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DairyUserListResponse;
import com.digitalconsultant.api.pojo.response.DairyUserResponse;
import com.digitalconsultant.api.pojo.response.Response;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public interface DairyUserService {

    DairyUserResponse addDairyUser(ApiRequestDto<AddDairyUserRequest> apiRequestDto);

    DairyUserListResponse getListOfDairyUsers(ApiRequestDto<ResourceListRequest> apiRequestDto);

    Response deleteUser(Long dairyUserId) throws Exception;
}
