package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.service.SessionService;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import com.digitalconsultant.persistence.dao.DDSessionEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private DDSessionEntityDao ddSessionEntityDao;

    @Override
    public DDSessionEntity getSessionDetails(String sessionToken, String deviceId) {
        DDSessionEntity ddSessionEntity;
        ddSessionEntity = ddSessionEntityDao.getSessionDetailsBySessionToken(sessionToken, deviceId);
        return ddSessionEntity;
    }
}
