package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DeductionMasterDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
public class DeductionMasterListResponse extends Response {
    private List<DeductionMasterDto> deductions;

    public List<DeductionMasterDto> getDeductions() {
        return deductions;
    }

    public void setDeductions(List<DeductionMasterDto> deductions) {
        this.deductions = deductions;
    }
}
