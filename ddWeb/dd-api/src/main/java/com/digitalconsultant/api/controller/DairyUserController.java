package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDairyUserRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DairyUserListResponse;
import com.digitalconsultant.api.pojo.response.DairyUserResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DairyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
@Controller
@Api(value = "Account Master API", description = SwaggerMessage.DAIRY_USER_CONTROLLER_DESCRIPTION)
public class DairyUserController extends AbstractController {

    @Autowired
    private DairyUserService dairyUserService;

    @RequestMapping(value = URI.ADD_USER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_USER_API_NAME, notes = SwaggerMessage.ADD_USER_API_DESCRIPTION, response = DairyUserResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DairyUserResponse addDairyUser(@RequestBody AddDairyUserRequest addDairyUserRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddDairyUserRequest> apiRequest = getApiRequest(addDairyUserRequest, httpServletRequest);
        DairyUserResponse dairyUserResponse = dairyUserService.addDairyUser(apiRequest);
        finalizeResponse(dairyUserResponse, httpServletRequest);
        return dairyUserResponse;
    }

    @RequestMapping(value = URI.LIST_USER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_USER_API_NAME, notes = SwaggerMessage.LIST_USER_API_DESCRIPTION, response = DairyUserResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DairyUserListResponse getListOfDairyUsers(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        DairyUserListResponse dairyUserListResponse = dairyUserService.getListOfDairyUsers(apiRequest);
        finalizeResponse(dairyUserListResponse, httpServletRequest);
        return dairyUserListResponse;
    }

    @RequestMapping(value = URI.DELETE_USER_URI, method = RequestMethod.DELETE)
    @ApiOperation(value = SwaggerMessage.DELETE_USER_API_NAME, notes = SwaggerMessage.DELETE_USER_API_DESCRIPTION, response = Response.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_USER_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    Response deleteDairyUserId(@PathVariable(name = URI.PATH_VARIABLE_USER_ID) Long dairyUserId, HttpServletRequest httpServletRequest) throws Exception {
        Response response = dairyUserService.deleteUser(dairyUserId);
        finalizeResponse(response, httpServletRequest);
        return response;
    }

}
