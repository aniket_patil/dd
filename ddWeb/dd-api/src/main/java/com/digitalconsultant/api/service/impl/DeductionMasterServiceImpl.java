package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.dto.DeductionMasterDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.AddDeductionMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeductionMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeductionMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DeductionMasterService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.AccountEntity;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import com.digitalconsultant.datamodel.entity.DeductionEntity;
import com.digitalconsultant.persistence.dao.AccountEntityDao;
import com.digitalconsultant.persistence.dao.DeductionEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
@Service
public class DeductionMasterServiceImpl implements DeductionMasterService {

    @Autowired
    private DeductionEntityDao deductionEntityDao;
    @Autowired
    private AccountEntityDao accountEntityDao;

    @Override
    public DeductionMasterResponse addDeduction(ApiRequestDto<AddDeductionMasterRequest> apiRequestDto) {
        AddDeductionMasterRequest addDeductionMasterRequest = apiRequestDto.getRequestData();

        DeductionEntity deductionEntity = new DeductionEntity();
        DatabaseCriteria<AccountEntity> databaseCriteria = new DatabaseCriteria<>(AccountEntity.class);
        databaseCriteria.setId(addDeductionMasterRequest.getAccountId());
        AccountEntity accountEntity = accountEntityDao.getEntity(databaseCriteria);
        deductionEntity.setAccountEntity(accountEntity);
        deductionEntity.setDeductionAmount(addDeductionMasterRequest.getDeductionAmount());
        deductionEntity.setDeductionCycle(addDeductionMasterRequest.getDeductionCycle());
        deductionEntityDao.insertEntity(deductionEntity);

        DeductionMasterDto deductionMasterDto = getDeductionMasterDto(deductionEntity);

        DeductionMasterResponse deductionMasterResponse = new DeductionMasterResponse();
        deductionMasterResponse.setDeductionDetails(deductionMasterDto);
        deductionMasterResponse.setMessage("message.success.deduction.add");

        return deductionMasterResponse;
    }


    @Override
    public DeductionMasterResponse updateDeduction(ApiRequestDto<AddDeductionMasterRequest> apiRequestDto) throws Exception {
        AddDeductionMasterRequest addDeductionMasterRequest = apiRequestDto.getRequestData();
        Long deductionId = (Long) apiRequestDto.getResourceId();
        DeductionEntity deductionEntity = getDeductionEntity(deductionId);
        if (deductionEntity == null) {
            throw new CustomException("error.invalid.deductionId");
        }

        AccountEntity accountEntity = deductionEntity.getAccountEntity();
        deductionEntity.setAccountEntity(accountEntity);
        deductionEntity.setDeductionAmount(addDeductionMasterRequest.getDeductionAmount());
        deductionEntity.setDeductionCycle(addDeductionMasterRequest.getDeductionCycle());
        deductionEntityDao.updateEntity(deductionEntity);

        DeductionMasterDto deductionMasterDto = getDeductionMasterDto(deductionEntity);
        DeductionMasterResponse deductionMasterResponse = new DeductionMasterResponse();
        deductionMasterResponse.setDeductionDetails(deductionMasterDto);
        deductionMasterResponse.setMessage("message.success.deduction.update");

        return deductionMasterResponse;
    }

    @Override
    public DeductionMasterListResponse getListOfDeductions(ApiRequestDto<ResourceListRequest> apiRequestDto) {
        DDSessionEntity ddSessionEntity = apiRequestDto.getDdSessionEntity();
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();

        DatabaseCriteria<DeductionEntity> databaseCriteria = new DatabaseCriteria<>(DeductionEntity.class);
        databaseCriteria.setLimit(resourceListRequest.getLimit());
        databaseCriteria.setOffset(resourceListRequest.getOffset());
        Long dairyId = ddSessionEntity.getDairyEntity().getId();
        List<DeductionEntity> deductionEntityList = deductionEntityDao.findDeductionsByDairyId(dairyId, databaseCriteria);

        DeductionMasterListResponse deductionMasterListResponse = new DeductionMasterListResponse();
        if (!ValidationUtils.isListNull(deductionEntityList)) {
            List<DeductionMasterDto> deductions = new ArrayList<>(deductionEntityList.size());
            for (Iterator<DeductionEntity> iterator = deductionEntityList.iterator(); iterator.hasNext(); ) {
                DeductionEntity deductionEntity = iterator.next();
                DeductionMasterDto deductionMasterDto = getDeductionMasterDto(deductionEntity);
                deductions.add(deductionMasterDto);
            }
            deductionMasterListResponse.setDeductions(deductions);
        }
        deductionMasterListResponse.setMessage("message.request.processed.success");

        return deductionMasterListResponse;
    }

    @Override
    public Response deleteDeduction(Long deductionId) throws Exception {
        DeductionEntity deductionEntity = getDeductionEntity(deductionId);
        if (deductionEntity == null) {
            throw new CustomException("error.invalid.deductionId");
        }
        deductionEntityDao.deleteEntity(deductionEntity);
        Response response = new Response();
        response.setMessage("message.success.deduction.delete");
        return response;
    }

    private DeductionEntity getDeductionEntity(Long deductionId) {
        DatabaseCriteria<DeductionEntity> databaseCriteria = new DatabaseCriteria<>(DeductionEntity.class);
        databaseCriteria.setId(deductionId);
        return deductionEntityDao.getEntity(databaseCriteria);
    }

    private DeductionMasterDto getDeductionMasterDto(DeductionEntity deductionEntity) {
        DeductionMasterDto deductionMasterDto = new DeductionMasterDto();
        deductionMasterDto.setDeductionAmount(deductionEntity.getDeductionAmount());
        deductionMasterDto.setAccountName(deductionEntity.getAccountEntity().getAccount());
        deductionMasterDto.setDeductionCycle(deductionEntity.getDeductionCycle().getValue());
        deductionMasterDto.setDeductionId(deductionEntity.getId());
        return deductionMasterDto;
    }
}
