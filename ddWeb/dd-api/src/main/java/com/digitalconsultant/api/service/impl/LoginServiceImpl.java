package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.ForgotPasswordRequest;
import com.digitalconsultant.api.pojo.request.LoginRequest;
import com.digitalconsultant.api.pojo.request.RegistrationRequest;
import com.digitalconsultant.api.pojo.response.LoginResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.LoginService;
import com.digitalconsultant.common.pojo.ConfigDtls;
import com.digitalconsultant.common.pojo.DairyDtls;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.DateUtils;
import com.digitalconsultant.common.util.RandomStringGenerator;
import com.digitalconsultant.datamodel.entity.*;
import com.digitalconsultant.datamodel.enumeration.PasswordStatus;
import com.digitalconsultant.datamodel.enumeration.UserRole;
import com.digitalconsultant.persistence.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private DeviceEntityDao deviceEntityDao;
    @Autowired
    private DDPasswordEntityDao ddPasswordEntityDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private DDSessionEntityDao ddSessionEntityDao;
    @Autowired
    private DairyEntityDao dairyEntityDao;
    @Autowired
    private DDConfigEntityDao ddConfigEntityDao;

    @Override
    public LoginResponse login(ApiRequestDto<LoginRequest> apiRequestDto) throws CustomException {
        LoginResponse loginResponse = null;
        LoginRequest loginRequest = apiRequestDto.getRequestData();
        DeviceEntity deviceEntity = deviceEntityDao.findDeviceByDeviceId(apiRequestDto.getDeviceId());

        if (deviceEntity != null) {
            String userName = loginRequest.getUserName();
            DDPasswordEntity ddPasswordEntity = ddPasswordEntityDao.getUserCredentialsByUserName(userName);
            if (ddPasswordEntity != null) {
                DairyEntity dairyEntity = ddPasswordEntity.getDairyEntity();
                PasswordStatus passwordStatus = ddPasswordEntity.getPasswordStatus();
                if (passwordStatus == PasswordStatus.ACTIVE) {
                    String rawPassword = loginRequest.getPassword();
                    String encodedPassword = ddPasswordEntity.getPassword();
                    boolean isPasswordMatches = bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
                    if (isPasswordMatches) {
                        DDSessionEntity ddSessionEntity = new DDSessionEntity();
                        ddSessionEntity.setDeviceId(deviceEntity.getDeviceId());
                        ddSessionEntity.setDairyEntity(ddPasswordEntity.getDairyEntity());
                        String sessionToken = RandomStringGenerator.generateUUID();
                        ddSessionEntity.setSessionToken(sessionToken);
                        ddSessionEntity.setUserName(userName);
                        ddSessionEntity.setShift(loginRequest.getShift());
                        ddSessionEntityDao.insertEntity(ddSessionEntity);
                        loginResponse = new LoginResponse();
                        loginResponse.setMessage("message.success.login");
                        loginResponse.setSessionToken(sessionToken);

                        loginResponse.setDairyName(dairyEntity.getName());
                        loginResponse.setDistrict(dairyEntity.getDistrictEntity().getDistrict());
                    } else {
                        throw new CustomException("error.password.invalid");
                    }
                } else {
                    throw new CustomException("error.password.expired");
                }


            } else {
                throw new CustomException("error.userName.invalid");
            }
        } else {
            throw new CustomException("error.invalid.deviceId");
        }

        return loginResponse;
    }

    @Override
    public Response registration(ApiRequestDto<RegistrationRequest> apiRequestDto) throws CustomException {
        Response response;
        RegistrationRequest registrationRequest = apiRequestDto.getRequestData();
        DairyDtls dairyDtls = registrationRequest.getDairyDetails();
        ConfigDtls configDtls = registrationRequest.getConfigDetails();

        String emailId = dairyDtls.getEmailId();
        String mobileNo = dairyDtls.getMobileNo();

        DairyEntity dairyEntity = new DairyEntity();
        DistrictEntity districtEntity = new DistrictEntity();
        districtEntity.setId(dairyDtls.getDistrictId());
        dairyEntity.setName(dairyDtls.getDairyName());
        dairyEntity.setDistrictEntity(districtEntity);
        dairyEntity.setEmailId(emailId);
        dairyEntity.setMobileNo(mobileNo);
        dairyEntity.setStatus(true);
        dairyEntityDao.insertEntity(dairyEntity);

        DDConfigEntity ddConfigEntity = new DDConfigEntity();
        ddConfigEntity.setDairyEntity(dairyEntity);
        Date billDate = DateUtils.parseDate(configDtls.getBillDate());
        ddConfigEntity.setBillSpan(configDtls.getBillSpan());
        ddConfigEntity.setBillDate(billDate);
        ddConfigEntityDao.insertEntity(ddConfigEntity);


        DDPasswordEntity ddPasswordEntity = new DDPasswordEntity();
        ddPasswordEntity.setDairyEntity(dairyEntity);
        String encodedPassword = bCryptPasswordEncoder.encode(dairyDtls.getPassword());
        ddPasswordEntity.setPassword(encodedPassword);
        ddPasswordEntity.setPasswordStatus(PasswordStatus.ACTIVE);
        ddPasswordEntity.setUserName(dairyDtls.getUserName());
        ddPasswordEntity.setUserRole(UserRole.ADMIN);
        ddPasswordEntityDao.insertEntity(ddPasswordEntity);

        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setDairyEntity(dairyEntity);
        deviceEntity.setDeviceId(apiRequestDto.getDeviceId());
        deviceEntityDao.insertEntity(deviceEntity);

        response = new Response();
        response.setMessage("message.success.dairyRegistration");

        //Send Message & Email
        return response;
    }

    @Override
    public Response forgotPassword(ApiRequestDto<ForgotPasswordRequest> apiRequestDto) throws Exception {
        String deviceId = apiRequestDto.getDeviceId();
        DeviceEntity deviceEntity = deviceEntityDao.findDeviceByDeviceId(deviceId);
        if (deviceEntity != null) {
            ForgotPasswordRequest forgotPasswordRequest = apiRequestDto.getRequestData();
            DDPasswordEntity ddPasswordEntity = new DDPasswordEntity();
            ddPasswordEntity.setUserName(forgotPasswordRequest.getUserName());
            String encodedPassword = bCryptPasswordEncoder.encode(forgotPasswordRequest.getNewPassword());
            ddPasswordEntity.setPassword(encodedPassword);
            boolean result = ddPasswordEntityDao.updatePassword(ddPasswordEntity);
            if (result) {
                Response response = new Response();
                response.setMessage("message.success.password.update");
                return response;
            } else {
                throw new CustomException("error.password.update");
            }
        } else {
            throw new CustomException("error.invalid.deviceId");
        }


    }

    @Override
    public Response logout(String sessionToken) {
        DDSessionEntity ddSessionEntity = new DDSessionEntity();
        ddSessionEntity.setSessionToken(sessionToken);
        ddSessionEntityDao.deleteEntity(ddSessionEntity);
        Response response = new Response();
        response.setMessage("message.logout.success");
        return response;
    }
}
