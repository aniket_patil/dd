package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.dto.DeviceMasterDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.AddDeductionMasterRequest;
import com.digitalconsultant.api.pojo.request.AddDeviceMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeviceMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeviceMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DeviceMasterService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import com.digitalconsultant.datamodel.entity.DeviceEntity;
import com.digitalconsultant.persistence.dao.DeviceEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
@Service
public class DeviceMasterServiceImpl implements DeviceMasterService {

    @Autowired
    private DeviceEntityDao deviceEntityDao;

    @Override
    public DeviceMasterResponse addDevice(ApiRequestDto<AddDeviceMasterRequest> apiRequestDto) {
        AddDeviceMasterRequest addDeviceMasterRequest = apiRequestDto.getRequestData();
        DDSessionEntity ddSessionEntity = apiRequestDto.getDdSessionEntity();

        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setDeviceId(addDeviceMasterRequest.getDeviceId());
        deviceEntity.setDairyEntity(ddSessionEntity.getDairyEntity());
        deviceEntityDao.insertEntity(deviceEntity);

        DeviceMasterResponse deviceMasterResponse = new DeviceMasterResponse();
        deviceMasterResponse.setMessage("message.success.device.add");
        DeviceMasterDto deviceMasterDto = getDeviceMasterDto(deviceEntity);
        deviceMasterResponse.setDeviceDetails(deviceMasterDto);

        return deviceMasterResponse;
    }


    @Override
    public DeviceMasterResponse updateDevice(ApiRequestDto<AddDeviceMasterRequest> apiRequestDto) throws Exception {
        AddDeviceMasterRequest addDeviceMasterRequest = apiRequestDto.getRequestData();
        Long id = (Long) apiRequestDto.getResourceId();
        DeviceEntity deviceEntity = getDeviceEntity(id);

        if (deviceEntity == null)
            throw new CustomException("error.device.notFound");

        String deviceId = addDeviceMasterRequest.getDeviceId();
        deviceEntity.setDeviceId(deviceId);
        deviceEntityDao.updateEntity(deviceEntity);

        DeviceMasterResponse deviceMasterResponse = new DeviceMasterResponse();
        deviceMasterResponse.setMessage("message.success.device.update");
        DeviceMasterDto deviceMasterDto = getDeviceMasterDto(deviceEntity);
        deviceMasterResponse.setDeviceDetails(deviceMasterDto);

        return deviceMasterResponse;
    }

    @Override
    public DeviceMasterListResponse getListOfDevices(ApiRequestDto<ResourceListRequest> apiRequestDto) {
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();

        DatabaseCriteria<DeviceEntity> databaseCriteria = new DatabaseCriteria<DeviceEntity>(DeviceEntity.class);
        databaseCriteria.setLimit(resourceListRequest.getLimit());
        databaseCriteria.setOffset(resourceListRequest.getOffset());

        List<DeviceEntity> deviceEntityList = deviceEntityDao.findDevicesByDairyId(apiRequestDto.getDairyId(), databaseCriteria);
        DeviceMasterListResponse deviceMasterListResponse = new DeviceMasterListResponse();
        deviceMasterListResponse.setMessage("message.request.processed.success");

        if (!ValidationUtils.isListNull(deviceEntityList)) {
            List<DeviceMasterDto> devices = new ArrayList<>(deviceEntityList.size());
            for (Iterator<DeviceEntity> iterator = deviceEntityList.iterator(); iterator.hasNext(); ) {
                DeviceEntity deviceEntity = iterator.next();
                DeviceMasterDto deviceMasterDto = getDeviceMasterDto(deviceEntity);
                devices.add(deviceMasterDto);
            }
            deviceMasterListResponse.setDevices(devices);
        }


        return deviceMasterListResponse;
    }

    @Override
    public Response deleteDevice(Long deviceId) throws Exception {
        DeviceEntity deviceEntity = getDeviceEntity(deviceId);
        if (deviceEntity == null)
            throw new CustomException("error.device.notFound");
        deviceEntityDao.deleteEntity(deviceEntity);
        Response response = new Response();
        response.setMessage("message.success.device.delete");
        return response;
    }

    private DeviceMasterDto getDeviceMasterDto(DeviceEntity deviceEntity) {
        DeviceMasterDto deviceMasterDto = new DeviceMasterDto();
        deviceMasterDto.setId(deviceEntity.getId());
        deviceMasterDto.setDeviceId(deviceEntity.getDeviceId());
        return deviceMasterDto;
    }

    private DeviceEntity getDeviceEntity(Long id) {
        DatabaseCriteria<DeviceEntity> databaseCriteria = new DatabaseCriteria<>(DeviceEntity.class);
        databaseCriteria.setId(id);
        DeviceEntity deviceEntity = deviceEntityDao.getEntity(databaseCriteria);
        return deviceEntity;
    }
}
