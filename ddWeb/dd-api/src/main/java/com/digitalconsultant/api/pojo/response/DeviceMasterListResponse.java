package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DeviceMasterDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public class DeviceMasterListResponse extends Response {
    private List<DeviceMasterDto> devices;

    public List<DeviceMasterDto> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceMasterDto> devices) {
        this.devices = devices;
    }
}
