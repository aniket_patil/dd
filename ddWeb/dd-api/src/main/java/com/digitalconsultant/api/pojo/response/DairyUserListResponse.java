package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DairyUserDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public class DairyUserListResponse extends Response {

    private List<DairyUserDto> dairyUsers;

    public List<DairyUserDto> getDairyUsers() {
        return dairyUsers;
    }

    public void setDairyUsers(List<DairyUserDto> dairyUsers) {
        this.dairyUsers = dairyUsers;
    }
}
