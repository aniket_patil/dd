package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */
public class AbstractController {

    @Autowired
    private ResourceBundleMessageSource resourceBundleMessageSource;


    protected String getMessage(String code, Object[] args, Locale locale) {
        String message = resourceBundleMessageSource.getMessage(code, args, locale);
        return message;
    }

    protected void finalizeResponse(Response response, HttpServletRequest httpServletRequest) {
        String message = getMessage(response.getMessage(), null, httpServletRequest.getLocale());
        response.setMessage(message);
    }

    protected <T> ApiRequestDto<T> getApiRequest(T requestData, HttpServletRequest httpServletRequest) {
        ApiRequestDto<T> apiRequestDto = new ApiRequestDto<T>();
        apiRequestDto.setRequestData(requestData);
        DDSessionEntity ddSessionEntity = (DDSessionEntity) httpServletRequest.getAttribute(Constants.SESSION_KEY_NAME);
        apiRequestDto.setDdSessionEntity(ddSessionEntity);
        String deviceId = httpServletRequest.getHeader(Constants.DEVICE_ID_SESSION_KEY_NAME);
        apiRequestDto.setDeviceId(deviceId);
        return apiRequestDto;
    }


}
