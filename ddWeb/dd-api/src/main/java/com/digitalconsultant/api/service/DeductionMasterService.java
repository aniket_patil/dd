package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDeductionMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeductionMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeductionMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
public interface DeductionMasterService {

    DeductionMasterResponse addDeduction(ApiRequestDto<AddDeductionMasterRequest> apiRequestDto);

    DeductionMasterResponse updateDeduction(ApiRequestDto<AddDeductionMasterRequest> apiRequestDto) throws Exception;

    DeductionMasterListResponse getListOfDeductions(ApiRequestDto<ResourceListRequest> apiRequestDto);

    Response deleteDeduction(Long deductionId) throws Exception;
}
