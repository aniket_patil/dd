package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.MilkTypeMasterDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
public class MilkTypeMasterListResponse extends Response {

    private List<MilkTypeMasterDto> milkTypes;

    public List<MilkTypeMasterDto> getMilkTypes() {
        return milkTypes;
    }

    public void setMilkTypes(List<MilkTypeMasterDto> milkTypes) {
        this.milkTypes = milkTypes;
    }
}
