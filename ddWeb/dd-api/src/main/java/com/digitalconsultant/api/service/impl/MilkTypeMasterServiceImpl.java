package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.dto.MilkTypeMasterDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.AddMilkTypeMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterListResponse;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.MilkTypeMasterService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.DairyEntity;
import com.digitalconsultant.datamodel.entity.MilkTypeEntity;
import com.digitalconsultant.persistence.dao.MilkTypeEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
@Service
public class MilkTypeMasterServiceImpl implements MilkTypeMasterService {
    @Autowired
    private MilkTypeEntityDao milkTypeEntityDao;

    @Override
    public MilkTypeMasterResponse addMilkType(ApiRequestDto<AddMilkTypeMasterRequest> apiRequestDto) {
        AddMilkTypeMasterRequest addMilkTypeMasterRequest = apiRequestDto.getRequestData();
        DairyEntity dairyEntity = apiRequestDto.getDdSessionEntity().getDairyEntity();

        MilkTypeEntity milkTypeEntity = new MilkTypeEntity();
        milkTypeEntity.setDairyEntity(dairyEntity);
        milkTypeEntity.setMilkType(addMilkTypeMasterRequest.getMilkType());
        milkTypeEntityDao.insertEntity(milkTypeEntity);

        MilkTypeMasterDto milkTypeMasterDto = getMilkTypeMasterDto(milkTypeEntity);

        MilkTypeMasterResponse milkTypeMasterResponse = new MilkTypeMasterResponse();
        milkTypeMasterResponse.setMessage("message.success.milkType.add");
        milkTypeMasterResponse.setMilkType(milkTypeMasterDto);

        return milkTypeMasterResponse;
    }

    @Override
    public MilkTypeMasterResponse updateMilkType(ApiRequestDto<AddMilkTypeMasterRequest> apiRequestDto) throws Exception {
        AddMilkTypeMasterRequest addMilkTypeMasterRequest = apiRequestDto.getRequestData();
        Long milkTypeId = (Long) apiRequestDto.getResourceId();
        MilkTypeEntity milkTypeEntity = milkTypeEntityDao.getMilkTypeEntity(milkTypeId);
        if (milkTypeEntity == null)
            throw new CustomException("error.invalid.milkTypeId");

        milkTypeEntity.setMilkType(addMilkTypeMasterRequest.getMilkType());
        milkTypeEntityDao.updateEntity(milkTypeEntity);
        MilkTypeMasterResponse milkTypeMasterResponse = new MilkTypeMasterResponse();
        milkTypeMasterResponse.setMessage("message.success.milkType.update");
        MilkTypeMasterDto milkTypeMasterDto = getMilkTypeMasterDto(milkTypeEntity);
        milkTypeMasterResponse.setMilkType(milkTypeMasterDto);

        return milkTypeMasterResponse;
    }


    @Override
    public MilkTypeMasterListResponse getListOfMilkTypes(ApiRequestDto<ResourceListRequest> apiRequestDto) {
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();
        Long dairyId = apiRequestDto.getDairyId();

        DatabaseCriteria<MilkTypeEntity> databaseCriteria = new DatabaseCriteria<>(MilkTypeEntity.class);
        databaseCriteria.setOffset(resourceListRequest.getOffset());
        databaseCriteria.setLimit(resourceListRequest.getLimit());

        List<MilkTypeEntity> milkTypeEntityList = milkTypeEntityDao.findMilkTypesByDairyId(dairyId, databaseCriteria);
        MilkTypeMasterListResponse milkTypeMasterListResponse = new MilkTypeMasterListResponse();

        if (!ValidationUtils.isListNull(milkTypeEntityList)) {
            List<MilkTypeMasterDto> milkTypes = new ArrayList<>(milkTypeEntityList.size());
            for (Iterator<MilkTypeEntity> iterator = milkTypeEntityList.iterator(); iterator.hasNext(); ) {
                MilkTypeEntity milkTypeEntity = iterator.next();
                MilkTypeMasterDto milkTypeMasterDto = getMilkTypeMasterDto(milkTypeEntity);
                milkTypes.add(milkTypeMasterDto);
            }
            milkTypeMasterListResponse.setMilkTypes(milkTypes);
        }

        milkTypeMasterListResponse.setMessage("message.request.processed.success");

        return milkTypeMasterListResponse;
    }

    @Override
    public Response deleteMilkType(Long milkTypeId) throws Exception {
        MilkTypeEntity milkTypeEntity = milkTypeEntityDao.getMilkTypeEntity(milkTypeId);
        if (milkTypeEntity == null)
            throw new CustomException("error.invalid.milkTypeId");
        milkTypeEntityDao.deleteEntity(milkTypeEntity);

        Response response = new Response();
        response.setMessage("message.success.milkType.delete");
        return response;
    }

    private MilkTypeMasterDto getMilkTypeMasterDto(MilkTypeEntity milkTypeEntity) {
        MilkTypeMasterDto milkTypeMasterDto = new MilkTypeMasterDto();
        milkTypeMasterDto.setMilkType(milkTypeEntity.getMilkType());
        milkTypeMasterDto.setMilkTypeId(milkTypeEntity.getId());
        return milkTypeMasterDto;
    }


}
