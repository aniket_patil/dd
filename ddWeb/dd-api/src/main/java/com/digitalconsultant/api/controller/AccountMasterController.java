package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.request.AddAccountMasterRequest;
import com.digitalconsultant.api.pojo.response.AccountMasterListResponse;
import com.digitalconsultant.api.pojo.response.AccountMasterResponse;
import com.digitalconsultant.api.service.AccountMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Controller
@Api(value = "Account Master API", description = SwaggerMessage.ACCOUNT_MASTER_CONTROLLER_DESCRIPTION, position = SwaggerMessage.ACCOUNT_MASTER_API_POSITION)
public class AccountMasterController extends AbstractController {


    @Autowired
    private AccountMasterService accountMasterService;

    @RequestMapping(value = URI.ADD_ACCOUNT_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_ACCOUNT_API_NAME, notes = SwaggerMessage.ADD_ACCOUNT_API_DESCRIPTION, response = AccountMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    AccountMasterResponse addAccount(@RequestBody AddAccountMasterRequest addAccountMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddAccountMasterRequest> apiRequest = getApiRequest(addAccountMasterRequest, httpServletRequest);
        AccountMasterResponse accountMasterResponse = accountMasterService.addAccount(apiRequest);
        finalizeResponse(accountMasterResponse, httpServletRequest);
        return accountMasterResponse;
    }


    @RequestMapping(value = URI.UPDATE_ACCOUNT_MASTER_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.UPDATE_ACCOUNT_API_NAME, notes = SwaggerMessage.UPDATE_ACCOUNT_API_DESCRIPTION, response = AccountMasterResponse.class, position = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_ACCOUNT_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    AccountMasterResponse updateAccount(@PathVariable(name = URI.PATH_VARIABLE_ACCOUNT_ID) Long accountId, @RequestBody AddAccountMasterRequest addAccountMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddAccountMasterRequest> apiRequest = getApiRequest(addAccountMasterRequest, httpServletRequest);
        apiRequest.setResourceId(accountId);
        AccountMasterResponse accountMasterResponse = accountMasterService.updateAccount(apiRequest);
        finalizeResponse(accountMasterResponse, httpServletRequest);
        return accountMasterResponse;
    }


    @RequestMapping(value = URI.LIST_ACCOUNT_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_ACCOUNT_API_NAME, notes = SwaggerMessage.LIST_ACCOUNTS_API_DESCRIPTION, response = AccountMasterResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    AccountMasterListResponse getListOfAccounts(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        AccountMasterListResponse accountMasterListResponse = accountMasterService.getListOfAccounts(apiRequest);
        finalizeResponse(accountMasterListResponse, httpServletRequest);
        return accountMasterListResponse;
    }


}
