package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.dto.BankAccountDto;
import com.digitalconsultant.api.dto.CustomerAccountDto;
import com.digitalconsultant.api.dto.CustomerDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.AddCustomerAccountRequest;
import com.digitalconsultant.api.pojo.request.AddCustomerRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.CustomerAccountResponse;
import com.digitalconsultant.api.pojo.response.CustomerListResponse;
import com.digitalconsultant.api.pojo.response.CustomerResponse;
import com.digitalconsultant.api.service.CustomerService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.DateUtils;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.*;
import com.digitalconsultant.persistence.dao.CustomerAccountEntityDao;
import com.digitalconsultant.persistence.dao.CustomerEntityDao;
import com.digitalconsultant.persistence.dao.MilkTypeEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerEntityDao customerEntityDao;
    @Autowired
    private MilkTypeEntityDao milkTypeEntityDao;
    @Autowired
    private CustomerAccountEntityDao customerAccountEntityDao;


    @Override
    public CustomerResponse getCustomerDetailsById(Long customerId) throws Exception {
        return null;
    }

    @Override
    public CustomerResponse addCustomer(ApiRequestDto<AddCustomerRequest> apiRequestDto) {
        AddCustomerRequest addCustomerRequest = apiRequestDto.getRequestData();
        DairyEntity dairyEntity = apiRequestDto.getDdSessionEntity().getDairyEntity();

        Date currentDate = DateUtils.getCurrentDate();
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setStatus(true);
        customerEntity.setDairyEntity(dairyEntity);
        customerEntity.setAddedOn(currentDate);
        customerEntity.setAdharNo(addCustomerRequest.getAdharNo());
        customerEntity.setCustomerName(addCustomerRequest.getCustomerName());
        customerEntity.setMobileNo(addCustomerRequest.getMobileNo());
        customerEntity.setUpdatedOn(currentDate);

        customerEntityDao.insertEntity(customerEntity);
        //String base64EncodedPhoto=addCustomerRequest.getPhotoBase64Encoded();
        CustomerDto customerDto = getCustomerDto(customerEntity);

        CustomerResponse customerResponse = new CustomerResponse();
        customerResponse.setMessage("message.success.customer.add");
        customerResponse.setCustomerDetails(customerDto);


        return customerResponse;
    }


    @Override
    public CustomerResponse updateCustomer(ApiRequestDto<AddCustomerRequest> apiRequestDto) throws Exception {
        AddCustomerRequest addCustomerRequest = apiRequestDto.getRequestData();
        Long customerId = (Long) apiRequestDto.getResourceId();
        CustomerEntity customerEntity = getCustomerEntity(customerId);

        if (customerEntity == null)
            throw new CustomException("error.invalid.customerId");
        customerEntity.setMobileNo(addCustomerRequest.getMobileNo());
        customerEntity.setAdharNo(addCustomerRequest.getAdharNo());
        customerEntity.setCustomerName(addCustomerRequest.getCustomerName());
        customerEntity.setUpdatedOn(DateUtils.getCurrentDate());

        customerEntityDao.updateEntity(customerEntity);
        CustomerDto customerDto = getCustomerDto(customerEntity);
        CustomerResponse customerResponse = new CustomerResponse();
        customerResponse.setMessage("message.success.customer.update");
        customerResponse.setCustomerDetails(customerDto);

        return customerResponse;
    }


    @Override
    public CustomerListResponse getListOfCustomers(ApiRequestDto<ResourceListRequest> apiRequestDto) {
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();
        DatabaseCriteria<CustomerEntity> databaseCriteria = new DatabaseCriteria<>(CustomerEntity.class);
        databaseCriteria.setOffset(resourceListRequest.getOffset());
        databaseCriteria.setLimit(resourceListRequest.getLimit());
        databaseCriteria.setSearchString(resourceListRequest.getSearchString());

        Long dairyId = apiRequestDto.getDairyId();
        List<CustomerEntity> customerEntityList = customerEntityDao.findCustomersByDairyId(dairyId, databaseCriteria);

        CustomerListResponse customerListResponse = new CustomerListResponse();
        customerListResponse.setMessage("message.request.processed.success");
        if (!ValidationUtils.isListNull(customerEntityList)) {
            Set<CustomerEntity> customerEntitySet = new HashSet<>(customerEntityList.size());
            for (Iterator<CustomerEntity> iterator = customerEntityList.iterator(); iterator.hasNext(); ) {
                CustomerEntity customerEntity = iterator.next();
                customerEntitySet.add(customerEntity);
            }
            List<CustomerDto> customers = new ArrayList<>(customerEntitySet.size());
            for (Iterator<CustomerEntity> iterator = customerEntitySet.iterator(); iterator.hasNext(); ) {
                CustomerEntity customerEntity = iterator.next();
                CustomerDto customerDto = getCustomerDto(customerEntity);
                customers.add(customerDto);
            }
            customerListResponse.setCustomers(customers);
        }

        return customerListResponse;
    }

    @Override
    public CustomerAccountResponse addAccount(ApiRequestDto<AddCustomerAccountRequest> apiRequestDto) throws Exception {
        AddCustomerAccountRequest addCustomerAccountRequest = apiRequestDto.getRequestData();
        Long customerId = addCustomerAccountRequest.getCustomerId();
        CustomerEntity customerEntity = getCustomerEntity(customerId);

        if (customerEntity == null)
            throw new CustomException("error.invalid.customerId");

        Date currentDate = DateUtils.getCurrentDate();
        CustomerAccountEntity customerAccountEntity = new CustomerAccountEntity();
        customerAccountEntity.setStatus(true);
        customerAccountEntity.setAccountNo(addCustomerAccountRequest.getAccountNo());
        customerAccountEntity.setAddedOn(currentDate);
        customerAccountEntity.setCustomerEntity(customerEntity);

        Long milkTypeId = addCustomerAccountRequest.getMilkTypeId();
        MilkTypeEntity milkTypeEntity = milkTypeEntityDao.getMilkTypeEntity(milkTypeId);

        if (milkTypeEntity == null)
            throw new CustomException("error.invalid.milkTypeId");
        customerAccountEntity.setMilkTypeEntity(milkTypeEntity);
        customerAccountEntity.setUpdatedOn(currentDate);
        BankAccountEntity bankAccountEntity = new BankAccountEntity();
        BankAccountDto bankAccountDto = addCustomerAccountRequest.getBankAccountDetails();
        bankAccountEntity.setAccountNo(bankAccountDto.getAccountNo());
        bankAccountEntity.setBankName(bankAccountDto.getBankName());
        bankAccountEntity.setBranchName(bankAccountDto.getBranchName());
        bankAccountEntity.setIfscCode(bankAccountDto.getIfscNo());
        customerAccountEntity.setBankAccountEntity(bankAccountEntity);

        customerAccountEntityDao.insertEntity(customerAccountEntity);
        CustomerAccountDto customerAccountDto = getCustomerAccountDto(customerAccountEntity);
        CustomerAccountResponse customerAccountResponse = new CustomerAccountResponse();
        customerAccountResponse.setMessage("message.success.customerAccount.add");
        customerAccountResponse.setAccountDetails(customerAccountDto);

        return customerAccountResponse;
    }

    private CustomerAccountDto getCustomerAccountDto(CustomerAccountEntity customerAccountEntity) {
        CustomerAccountDto customerAccountDto = new CustomerAccountDto();
        customerAccountDto.setCustomerName(customerAccountEntity.getCustomerEntity().getCustomerName());
        customerAccountDto.setAccountId(customerAccountEntity.getId());
        customerAccountDto.setAccountNo(customerAccountEntity.getAccountNo());
        customerAccountDto.setMilkType(customerAccountEntity.getMilkTypeEntity().getMilkType());
        customerAccountDto.setMilkTypeId(customerAccountEntity.getMilkTypeEntity().getId());
        BankAccountEntity bankAccountEntity = customerAccountEntity.getBankAccountEntity();
        if (bankAccountEntity != null) {
            BankAccountDto bankAccountDto = new BankAccountDto();
            bankAccountDto.setAccountNo(bankAccountEntity.getAccountNo());
            bankAccountDto.setBankName(bankAccountEntity.getBankName());
            bankAccountDto.setBranchName(bankAccountEntity.getBranchName());
            bankAccountDto.setIfscNo(bankAccountEntity.getIfscCode());
            customerAccountDto.setBankAccountDetails(bankAccountDto);
        }
        return customerAccountDto;
    }

    private CustomerEntity getCustomerEntity(Long customerId) {
        DatabaseCriteria<CustomerEntity> databaseCriteria = new DatabaseCriteria<>(CustomerEntity.class);
        databaseCriteria.setId(customerId);
        return customerEntityDao.findCustomerEntityByCriteria(databaseCriteria);
    }

    private CustomerDto getCustomerDto(CustomerEntity customerEntity) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setAdharNo(customerEntity.getAdharNo());
        customerDto.setMobileNo(customerEntity.getMobileNo());
        customerDto.setCustomerName(customerEntity.getCustomerName());
        customerDto.setAddedOn(DateUtils.formatDate(customerEntity.getAddedOn()));
        customerDto.setCustomerId(customerEntity.getId());

        List<CustomerAccountEntity> customerAccountEntities = customerEntity.getCustomerAccountEntities();
        if (!ValidationUtils.isListNull(customerAccountEntities)) {
            List<CustomerAccountDto> customerAccounts = new ArrayList<>(customerAccountEntities.size());
            for (Iterator<CustomerAccountEntity> iterator = customerAccountEntities.iterator(); iterator.hasNext(); ) {
                CustomerAccountEntity customerAccountEntity = iterator.next();
                CustomerAccountDto customerAccountDto = getCustomerAccountDto(customerAccountEntity);
                customerAccounts.add(customerAccountDto);
            }
            customerDto.setCustomerAccounts(customerAccounts);
        }
        return customerDto;
    }
}
