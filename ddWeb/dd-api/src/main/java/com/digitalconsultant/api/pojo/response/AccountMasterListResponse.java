package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.AccountMasterDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public class AccountMasterListResponse extends Response {
    private List<AccountMasterDto> accounts;

    public List<AccountMasterDto> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountMasterDto> accounts) {
        this.accounts = accounts;
    }
}
