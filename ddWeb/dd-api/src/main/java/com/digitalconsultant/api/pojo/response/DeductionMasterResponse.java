package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DeductionMasterDto;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
public class DeductionMasterResponse extends Response {
    private DeductionMasterDto deductionDetails;

    public DeductionMasterDto getDeductionDetails() {
        return deductionDetails;
    }

    public void setDeductionDetails(DeductionMasterDto deductionDetails) {
        this.deductionDetails = deductionDetails;
    }
}
