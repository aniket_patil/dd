package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.CustomerDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
public class CustomerListResponse extends Response {

    private List<CustomerDto> customers;

    public List<CustomerDto> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerDto> customers) {
        this.customers = customers;
    }
}
