package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.CustomerAccountDto;
import com.digitalconsultant.common.dto.AccountDto;

/**
 * Created by aniket.patil_3887 on 2/20/2017.
 */
public class CustomerAccountResponse extends Response {
    private CustomerAccountDto accountDetails;

    public CustomerAccountDto getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(CustomerAccountDto accountDetails) {
        this.accountDetails = accountDetails;
    }
}
