package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.request.AddAccountMasterRequest;
import com.digitalconsultant.api.pojo.response.AccountMasterListResponse;
import com.digitalconsultant.api.pojo.response.AccountMasterResponse;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface AccountMasterService {

    AccountMasterResponse addAccount(ApiRequestDto<AddAccountMasterRequest> apiRequestDto);

    AccountMasterResponse updateAccount(ApiRequestDto<AddAccountMasterRequest> apiRequestDto);

    AccountMasterListResponse getListOfAccounts(ApiRequestDto<ResourceListRequest> apiRequestDto);
}
