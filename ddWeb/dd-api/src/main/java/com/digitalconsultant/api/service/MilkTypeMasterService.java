package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddMilkTypeMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterListResponse;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
public interface MilkTypeMasterService {

    MilkTypeMasterResponse addMilkType(ApiRequestDto<AddMilkTypeMasterRequest> apiRequestDto);

    MilkTypeMasterResponse updateMilkType(ApiRequestDto<AddMilkTypeMasterRequest> apiRequestDto) throws Exception;

    MilkTypeMasterListResponse getListOfMilkTypes(ApiRequestDto<ResourceListRequest> apiRequestDto);

    Response deleteMilkType(Long milkTypeId) throws Exception;
}
