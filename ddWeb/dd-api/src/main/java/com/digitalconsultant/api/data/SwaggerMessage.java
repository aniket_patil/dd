package com.digitalconsultant.api.data;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */
public class SwaggerMessage {

    //Description
    public static final String LOGIN_CONTROLLER_DESCRIPTION = " Login and  Registration";
    public static final String ACCOUNT_MASTER_CONTROLLER_DESCRIPTION = "Add,Update and List accounts";
    public static final String DEDUCTION_MASTER_CONTROLLER_DESCRIPTION = "Add,Update and List deductions";
    public static final String DEVICE_MASTER_CONTROLLER_DESCRIPTION = "Add,Update and List devices";
    public static final String DAIRY_USER_CONTROLLER_DESCRIPTION = "Add,Delete and List users";
    public static final String MILK_TYPE_MASTER_CONTROLLER_DESCRIPTION = "Add,Delete and List milk types";
    public static final String CUSTOMER_CONTROLLER_DESCRIPTION = "Add,Delete and List customers and customer accounts";

    public static final String REGISTRATION_API_DESCRIPTION = "This operation does registration of . After registration all masters get filled with default values.";
    public static final String FORGOT_PASSWORD_API_DESCRIPTION = "Forgot Password API";
    public static final String LOGIN_API_DESCRIPTION = "This operation does authentication of user. Validate userName & password and returns session token if success";
    public static final String LOGOUT_API_DESCRIPTION = "This operation sign outs user and invalidate session token";

    public static final String ADD_ACCOUNT_API_DESCRIPTION = "This api creates a new account";
    public static final String UPDATE_ACCOUNT_API_DESCRIPTION = "This api updates existing account";
    public static final String LIST_ACCOUNTS_API_DESCRIPTION = "This api returns list of accounts";

    public static final String ADD_CUSTOMER_API_DESCRIPTION = "This api creates a new customer";
    public static final String UPDATE_CUSTOMER_API_DESCRIPTION = "This api updates existing customer";
    public static final String LIST_CUSTOMERS_API_DESCRIPTION = "This api returns list of customers";

    public static final String ADD_DEDUCTION_API_DESCRIPTION = "This api creates a new deduction";
    public static final String UPDATE_DEDUCTION_API_DESCRIPTION = "This api updates existing deduction";
    public static final String LIST_DEDUCTION_API_DESCRIPTION = "This api returns list of deductions";
    public static final String DELETE_DEDUCTION_API_DESCRIPTION = "This api deletes deduction by deduction Id";


    public static final String ADD_DEVICE_API_DESCRIPTION = "This api creates a new device";
    public static final String UPDATE_DEVICE_API_DESCRIPTION = "This api updates existing device";
    public static final String LIST_DEVICE_API_DESCRIPTION = "This api returns list of devices";
    public static final String DELETE_DEVICE_API_DESCRIPTION = "This api deletes device by device Id";


    public static final String ADD_MILK_TYPE_API_DESCRIPTION = "This api creates a new milk type";
    public static final String UPDATE_MILK_TYPE_API_DESCRIPTION = "This api updates existing milk type";
    public static final String LIST_MILK_TYPE_API_DESCRIPTION = "This api returns list of milktypes";
    public static final String DELETE_MILK_TYPE_API_DESCRIPTION = "This api deletes device by milkType Id";

    public static final String ADD_USER_API_DESCRIPTION = "This api creates a new user";
    public static final String LIST_USER_API_DESCRIPTION = "This api returns list of users";
    public static final String DELETE_USER_API_DESCRIPTION = "This api deletes user by user Id";

    public static final String ADD_CUSTOMER_ACCOUNT_API_DESCRIPTION = "This api creates a new customer";


    //API Names
    public static final String LOGIN_API_NAME = "Login API";
    public static final String FORGOT_PASWORD_API_NAME = "Forgot Password API";
    public static final String LOGOUT_API_NAME = "Logout API";
    public static final String REGISTRATION_API_NAME = " Registration API";

    public static final String ADD_ACCOUNT_API_NAME = "Add Account API";
    public static final String UPDATE_ACCOUNT_API_NAME = "Update Account API";
    public static final String LIST_ACCOUNT_API_NAME = "List Account API";



    public static final String ADD_CUSTOMER_ACCOUNT_API_NAME = "Add Customer Account API";


    public static final String ADD_CUSTOMER_API_NAME = "Add Customer API";
    public static final String UPDATE_CUSTOMER_API_NAME = "Update Customer API";
    public static final String LIST_CUSTOMER_API_NAME = "List Customers API";

    public static final String ADD_DEDUCTION_API_NAME = "Add Deduction API";
    public static final String UPDATE_DEDUCTION_API_NAME = "Update Deduction API";
    public static final String LIST_DEDUCTION_API_NAME = "List Deduction API";
    public static final String DELETE_DEDUCTION_API_NAME = "Delete Deduction API";

    public static final String ADD_MILK_TYPE_API_NAME = "Add MilkType API";
    public static final String UPDATE_MILK_TYPE_API_NAME = "Update MilkType API";
    public static final String LIST_MILK_TYPE_API_NAME = "List MilkType API";
    public static final String DELETE_MILK_TYPE_API_NAME = "Delete MilkType API";

    public static final String ADD_DEVICE_API_NAME = "Add Deduction API";
    public static final String UPDATE_DEVICE_API_NAME = "Update Deduction API";
    public static final String LIST_DEVICE_API_NAME = "List Deduction API";
    public static final String DELETE_DEVICE_API_NAME = "Delete Deduction API";

    public static final String ADD_USER_API_NAME = "Add User API";
    public static final String LIST_USER_API_NAME = "List Users API";
    public static final String DELETE_USER_API_NAME = "Delete User API";


    //Position
    public static final int LOGIN_REGISTRATION_API_POSITION = 0;
    public static final int ACCOUNT_MASTER_API_POSITION = 1;

}
