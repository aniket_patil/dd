package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.ForgotPasswordRequest;
import com.digitalconsultant.api.pojo.request.LoginRequest;
import com.digitalconsultant.api.pojo.request.RegistrationRequest;
import com.digitalconsultant.api.pojo.response.LoginResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */


@Controller
@Api(value = "Login-Registration API", description = SwaggerMessage.LOGIN_CONTROLLER_DESCRIPTION, position = SwaggerMessage.LOGIN_REGISTRATION_API_POSITION)
public class LoginController extends AbstractController {

    @Autowired
    private LoginService loginService;


    @RequestMapping(value = URI.REGISTRATION_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.REGISTRATION_API_NAME, notes = SwaggerMessage.REGISTRATION_API_DESCRIPTION, response = Response.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    Response registration(@RequestBody RegistrationRequest registrationRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<RegistrationRequest> registrationRequestApiRequestDto = getApiRequest(registrationRequest, httpServletRequest);
        Response response = loginService.registration(registrationRequestApiRequestDto);
        finalizeResponse(response, httpServletRequest);
        return response;
    }


    @RequestMapping(value = URI.LOGIN_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LOGIN_API_NAME, notes = SwaggerMessage.LOGIN_API_DESCRIPTION, response = Response.class, position = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true)})
    public
    @ResponseBody
    LoginResponse login(@RequestBody LoginRequest loginRequest, HttpServletRequest httpServletRequest) throws CustomException {
        ApiRequestDto<LoginRequest> loginRequestApiRequestDto = getApiRequest(loginRequest, httpServletRequest);
        LoginResponse loginResponse = loginService.login(loginRequestApiRequestDto);
        finalizeResponse(loginResponse, httpServletRequest);
        return loginResponse;
    }


    @RequestMapping(value = URI.FORGOT_PASSWORD_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.FORGOT_PASWORD_API_NAME, notes = SwaggerMessage.FORGOT_PASSWORD_API_DESCRIPTION, response = Response.class, position = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true)})
    public
    @ResponseBody
    Response forgotPassword(@RequestHeader(name = Constants.DEVICE_ID_SESSION_KEY_NAME, required = true) String deviceId, @RequestBody ForgotPasswordRequest forgotPasswordRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ForgotPasswordRequest> apiRequest = getApiRequest(forgotPasswordRequest, httpServletRequest);
        apiRequest.setDeviceId(deviceId);
        Response response = loginService.forgotPassword(apiRequest);
        finalizeResponse(response, httpServletRequest);
        return response;
    }

    @RequestMapping(value = URI.LOGOUT_URI, method = RequestMethod.DELETE)
    @ApiOperation(value = SwaggerMessage.LOGOUT_API_NAME, notes = SwaggerMessage.LOGOUT_API_DESCRIPTION, response = Response.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true)})
    public
    @ResponseBody
    Response logout(@RequestHeader(name = Constants.API_KEY_NAME, required = true) String sessionToken, HttpServletRequest httpServletRequest) throws CustomException {
        Response logoutResponse = loginService.logout(sessionToken);
        finalizeResponse(logoutResponse, httpServletRequest);
        return logoutResponse;
    }


}
