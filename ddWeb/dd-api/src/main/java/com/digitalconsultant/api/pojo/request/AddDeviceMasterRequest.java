package com.digitalconsultant.api.pojo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
@ApiModel("Add/Update Device Request")
public class AddDeviceMasterRequest extends Request {

    @ApiModelProperty(name = "deviceId", required = true)
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
