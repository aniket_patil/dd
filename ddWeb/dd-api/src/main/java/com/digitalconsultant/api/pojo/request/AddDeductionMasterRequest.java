package com.digitalconsultant.api.pojo.request;

import com.digitalconsultant.datamodel.enumeration.DeductionCycle;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */

@ApiModel("Deduction Master Request")
public class AddDeductionMasterRequest extends Request {

    @ApiModelProperty(name = "accountId", required = true)
    private Long accountId;
    @ApiModelProperty(name = "deductionAmount", required = true)
    private Double deductionAmount;
    @ApiModelProperty(name = "deductionCycle", required = true)
    private DeductionCycle deductionCycle;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Double getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(Double deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    public DeductionCycle getDeductionCycle() {
        return deductionCycle;
    }

    public void setDeductionCycle(DeductionCycle deductionCycle) {
        this.deductionCycle = deductionCycle;
    }
}
