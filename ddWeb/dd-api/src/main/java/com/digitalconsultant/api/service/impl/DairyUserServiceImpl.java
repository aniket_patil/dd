package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.dto.DairyUserDto;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.pojo.request.AddDairyUserRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DairyUserListResponse;
import com.digitalconsultant.api.pojo.response.DairyUserResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DairyUserService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.DDPasswordEntity;
import com.digitalconsultant.datamodel.entity.DairyEntity;
import com.digitalconsultant.datamodel.enumeration.PasswordStatus;
import com.digitalconsultant.persistence.dao.DDPasswordEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
@Service
public class DairyUserServiceImpl implements DairyUserService {

    @Autowired
    private DDPasswordEntityDao ddPasswordEntityDao;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public DairyUserResponse addDairyUser(ApiRequestDto<AddDairyUserRequest> apiRequestDto) {
        AddDairyUserRequest addDairyUserRequest = apiRequestDto.getRequestData();
        DairyEntity dairyEntity = apiRequestDto.getDdSessionEntity().getDairyEntity();

        DDPasswordEntity ddPasswordEntity = new DDPasswordEntity();
        String encodedPassword = bCryptPasswordEncoder.encode(addDairyUserRequest.getPassword());
        ddPasswordEntity.setPassword(encodedPassword);
        ddPasswordEntity.setUserName(addDairyUserRequest.getUserName());
        ddPasswordEntity.setUserRole(addDairyUserRequest.getUserRole());
        ddPasswordEntity.setDairyEntity(dairyEntity);
        ddPasswordEntity.setPasswordStatus(PasswordStatus.ACTIVE);
        ddPasswordEntityDao.insertEntity(ddPasswordEntity);

        DairyUserDto dairyUserDto = getDairyUserDto(ddPasswordEntity);
        DairyUserResponse dairyUserResponse = new DairyUserResponse();
        dairyUserResponse.setMessage("message.success.dairyUser.add");
        dairyUserResponse.setUserDetails(dairyUserDto);

        return dairyUserResponse;
    }


    @Override
    public DairyUserListResponse getListOfDairyUsers(ApiRequestDto<ResourceListRequest> apiRequestDto) {
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();
        DatabaseCriteria<DDPasswordEntity> databaseCriteria = new DatabaseCriteria<>(DDPasswordEntity.class);
        databaseCriteria.setLimit(resourceListRequest.getLimit());
        databaseCriteria.setOffset(resourceListRequest.getOffset());
        Long dairyId = apiRequestDto.getDairyId();

        List<DDPasswordEntity> ddPasswordEntityList = ddPasswordEntityDao.findDairyUsersByDairyId(dairyId, databaseCriteria);
        DairyUserListResponse dairyUserListResponse = new DairyUserListResponse();
        if (!ValidationUtils.isListNull(ddPasswordEntityList)) {
            List<DairyUserDto> dairyUsers = new ArrayList<>(ddPasswordEntityList.size());
            for (Iterator<DDPasswordEntity> iterator = ddPasswordEntityList.iterator(); iterator.hasNext(); ) {
                DDPasswordEntity ddPasswordEntity = iterator.next();
                DairyUserDto dairyUserDto = getDairyUserDto(ddPasswordEntity);
                dairyUsers.add(dairyUserDto);
            }

            dairyUserListResponse.setDairyUsers(dairyUsers);
        }
        dairyUserListResponse.setMessage("message.request.processed.success");

        return dairyUserListResponse;
    }

    @Override
    public Response deleteUser(Long dairyUserId) throws Exception {
        DatabaseCriteria<DDPasswordEntity> databaseCriteria = new DatabaseCriteria<>(DDPasswordEntity.class);
        databaseCriteria.setId(dairyUserId);

        DDPasswordEntity ddPasswordEntity = ddPasswordEntityDao.getEntity(databaseCriteria);
        if (ddPasswordEntity == null)
            throw new CustomException("error.invalid.dairyUserId");

        ddPasswordEntityDao.deleteEntity(ddPasswordEntity);
        Response response = new Response();
        response.setMessage("message.success.dairyUser.delete");

        return response;
    }

    private DairyUserDto getDairyUserDto(DDPasswordEntity ddPasswordEntity) {
        DairyUserDto dairyUserDto = new DairyUserDto();
        dairyUserDto.setPasswordStatus(ddPasswordEntity.getPasswordStatus());
        dairyUserDto.setUserRole(ddPasswordEntity.getUserRole());
        dairyUserDto.setUserName(ddPasswordEntity.getUserName());
        dairyUserDto.setUserId(ddPasswordEntity.getId());
        return dairyUserDto;
    }
}
