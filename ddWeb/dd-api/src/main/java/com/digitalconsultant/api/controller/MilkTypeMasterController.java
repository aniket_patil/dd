package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddMilkTypeMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterListResponse;
import com.digitalconsultant.api.pojo.response.MilkTypeMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.MilkTypeMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
@Controller
@Api(value = "Milk Type Master API", description = SwaggerMessage.MILK_TYPE_MASTER_CONTROLLER_DESCRIPTION)
public class MilkTypeMasterController extends AbstractController {

    @Autowired
    private MilkTypeMasterService milkTypeMasterService;

    @RequestMapping(value = URI.ADD_MILK_TYPE_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_MILK_TYPE_API_NAME, notes = SwaggerMessage.ADD_MILK_TYPE_API_DESCRIPTION, response = MilkTypeMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    MilkTypeMasterResponse addMilkType(@RequestBody AddMilkTypeMasterRequest addMilkTypeMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddMilkTypeMasterRequest> apiRequest = getApiRequest(addMilkTypeMasterRequest, httpServletRequest);
        MilkTypeMasterResponse MilkTypeMasterResponse = milkTypeMasterService.addMilkType(apiRequest);
        finalizeResponse(MilkTypeMasterResponse, httpServletRequest);
        return MilkTypeMasterResponse;
    }

    @RequestMapping(value = URI.UPDATE_MILK_TYPE_MASTER_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.UPDATE_MILK_TYPE_API_NAME, notes = SwaggerMessage.UPDATE_MILK_TYPE_API_DESCRIPTION, response = MilkTypeMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_MILK_TYPE_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    MilkTypeMasterResponse updateMilkType(@PathVariable Long milkTypeId, @RequestBody AddMilkTypeMasterRequest addMilkTypeMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddMilkTypeMasterRequest> apiRequest = getApiRequest(addMilkTypeMasterRequest, httpServletRequest);
        apiRequest.setResourceId(milkTypeId);
        MilkTypeMasterResponse milkTypeMasterResponse = milkTypeMasterService.updateMilkType(apiRequest);
        finalizeResponse(milkTypeMasterResponse, httpServletRequest);
        return milkTypeMasterResponse;
    }

    @RequestMapping(value = URI.LIST_MILK_TYPE_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_MILK_TYPE_API_NAME, notes = SwaggerMessage.LIST_MILK_TYPE_API_DESCRIPTION, response = MilkTypeMasterListResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    MilkTypeMasterListResponse getListOfMilkTypes(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        MilkTypeMasterListResponse milkTypeMasterListResponse = milkTypeMasterService.getListOfMilkTypes(apiRequest);
        finalizeResponse(milkTypeMasterListResponse, httpServletRequest);
        return milkTypeMasterListResponse;
    }


    @RequestMapping(value = URI.DELETE_MILK_TYPE_MASTER_URI, method = RequestMethod.DELETE)
    @ApiOperation(value = SwaggerMessage.DELETE_MILK_TYPE_API_NAME, notes = SwaggerMessage.DELETE_MILK_TYPE_API_DESCRIPTION, response = Response.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_MILK_TYPE_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    Response deleteMilkType(@PathVariable(name = URI.PATH_VARIABLE_MILK_TYPE_ID) Long milkTypeId, HttpServletRequest httpServletRequest) throws Exception {
        Response response = milkTypeMasterService.deleteMilkType(milkTypeId);
        finalizeResponse(response, httpServletRequest);
        return response;
    }
}
