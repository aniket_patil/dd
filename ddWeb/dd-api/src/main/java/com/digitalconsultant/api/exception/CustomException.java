package com.digitalconsultant.api.exception;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public class CustomException extends Exception {

    private String messageCode;

    public CustomException(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

}
