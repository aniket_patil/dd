package com.digitalconsultant.api.exception;

import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.common.enumeration.Error;
import com.digitalconsultant.common.enumeration.ResponseCode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @Autowired
    private ResourceBundleMessageSource messageSource;
    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler(CustomException.class)
    public void handleCustomException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, CustomException e) throws Exception {
        Response response = new Response();
        response.setError(Error.BUSINESS_ERROR);
        response.setResponseCode(ResponseCode.ERROR);
        String code = e.getMessageCode();
        String message = messageSource.getMessage(code, null, httpServletRequest.getLocale());
        response.setMessage(message);
        httpServletResponse.getWriter().print(objectMapper.writeValueAsString(response));
    }

    @ExceptionHandler(HibernateException.class)
    public void handleSQLGrammerException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, HibernateException e) throws Exception {
        Response response = new Response();
        response.setError(Error.DB_EXCEPTION);
        response.setResponseCode(ResponseCode.ERROR);
        String message = e.getMessage();
        response.setMessage(message);
        httpServletResponse.getWriter().print(objectMapper.writeValueAsString(response));
    }


}
