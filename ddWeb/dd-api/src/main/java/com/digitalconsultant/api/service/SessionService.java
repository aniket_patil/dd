package com.digitalconsultant.api.service;

import com.digitalconsultant.datamodel.entity.DDSessionEntity;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface SessionService {

    DDSessionEntity getSessionDetails(String sessionToken, String deviceId);
}
