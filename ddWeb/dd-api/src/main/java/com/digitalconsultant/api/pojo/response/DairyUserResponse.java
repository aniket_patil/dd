package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DairyUserDto;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public class DairyUserResponse extends Response {
    DairyUserDto userDetails;

    public DairyUserDto getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(DairyUserDto userDetails) {
        this.userDetails = userDetails;
    }
}
