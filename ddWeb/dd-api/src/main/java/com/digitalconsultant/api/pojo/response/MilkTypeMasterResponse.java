package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.MilkTypeMasterDto;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
public class MilkTypeMasterResponse extends Response {

    private MilkTypeMasterDto milkType;

    public MilkTypeMasterDto getMilkType() {
        return milkType;
    }

    public void setMilkType(MilkTypeMasterDto milkType) {
        this.milkType = milkType;
    }
}
