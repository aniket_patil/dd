package com.digitalconsultant.api.pojo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@ApiModel(value = "Resource List Request")
public class ResourceListRequest extends Request {

    @ApiModelProperty(name = "offset")
    private Integer offset;
    @ApiModelProperty(name = "limit")
    private Integer limit;
    @ApiModelProperty(name = "searchString")
    private String searchString;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }
}
