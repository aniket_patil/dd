package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDeviceMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeviceMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeviceMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DeviceMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
@Controller
@Api(value = "Device Master API", description = SwaggerMessage.DEVICE_MASTER_CONTROLLER_DESCRIPTION)
public class DeviceMasterController extends AbstractController {

    @Autowired
    private DeviceMasterService deviceMasterService;

    @RequestMapping(value = URI.ADD_DEVICE_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_DEVICE_API_NAME, notes = SwaggerMessage.ADD_DEVICE_API_DESCRIPTION, response = DeviceMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DeviceMasterResponse addDevice(@RequestBody AddDeviceMasterRequest addDeviceMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddDeviceMasterRequest> apiRequest = getApiRequest(addDeviceMasterRequest, httpServletRequest);
        DeviceMasterResponse deviceMasterResponse = deviceMasterService.addDevice(apiRequest);
        finalizeResponse(deviceMasterResponse, httpServletRequest);
        return deviceMasterResponse;
    }


    @RequestMapping(value = URI.UPDATE_DEVICE_MASTER_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.UPDATE_DEVICE_API_NAME, notes = SwaggerMessage.UPDATE_DEVICE_API_DESCRIPTION, response = DeviceMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_DEVICE_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    DeviceMasterResponse updateDevice(@PathVariable Long deviceId, @RequestBody AddDeviceMasterRequest addDeviceMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddDeviceMasterRequest> apiRequest = getApiRequest(addDeviceMasterRequest, httpServletRequest);
        apiRequest.setResourceId(deviceId);
        DeviceMasterResponse deviceMasterResponse = deviceMasterService.updateDevice(apiRequest);
        finalizeResponse(deviceMasterResponse, httpServletRequest);
        return deviceMasterResponse;
    }

    @RequestMapping(value = URI.LIST_DEVICE_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_DEVICE_API_NAME, notes = SwaggerMessage.LIST_DEVICE_API_DESCRIPTION, response = DeviceMasterListResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DeviceMasterListResponse getListOfDevices(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        DeviceMasterListResponse deviceMasterListResponse = deviceMasterService.getListOfDevices(apiRequest);
        finalizeResponse(deviceMasterListResponse, httpServletRequest);
        return deviceMasterListResponse;
    }

    @RequestMapping(value = URI.DELETE_DEVICE_MASTER_URI, method = RequestMethod.DELETE)
    @ApiOperation(value = SwaggerMessage.DELETE_DEVICE_API_NAME, notes = SwaggerMessage.DELETE_DEVICE_API_DESCRIPTION, response = Response.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_DEVICE_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    Response deleteDevice(@PathVariable(name = URI.PATH_VARIABLE_DEVICE_ID) Long DeviceId, HttpServletRequest httpServletRequest) throws Exception {
        Response response = deviceMasterService.deleteDevice(DeviceId);
        finalizeResponse(response, httpServletRequest);
        return response;
    }


}
