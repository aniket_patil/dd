package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddCustomerAccountRequest;
import com.digitalconsultant.api.pojo.request.AddCustomerRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.CustomerAccountResponse;
import com.digitalconsultant.api.pojo.response.CustomerListResponse;
import com.digitalconsultant.api.pojo.response.CustomerResponse;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
public interface CustomerService {


    CustomerResponse getCustomerDetailsById(Long customerId) throws Exception;

    CustomerResponse addCustomer(ApiRequestDto<AddCustomerRequest> apiRequestDto);

    CustomerResponse updateCustomer(ApiRequestDto<AddCustomerRequest> apiRequestDto) throws Exception;

    CustomerListResponse getListOfCustomers(ApiRequestDto<ResourceListRequest> apiRequestDto);

    CustomerAccountResponse addAccount(ApiRequestDto<AddCustomerAccountRequest> apiRequestDto) throws Exception;


}
