package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDeductionMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeductionMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeductionMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;
import com.digitalconsultant.api.service.DeductionMasterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
@Controller
@Api(value = "Deduction Master API", description = SwaggerMessage.DEDUCTION_MASTER_CONTROLLER_DESCRIPTION)
public class DeductionMasterController extends AbstractController {

    @Autowired
    private DeductionMasterService deductionMasterService;

    @RequestMapping(value = URI.ADD_DEDUCTION_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_DEDUCTION_API_NAME, notes = SwaggerMessage.ADD_DEDUCTION_API_DESCRIPTION, response = DeductionMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DeductionMasterResponse addDeduction(@RequestBody AddDeductionMasterRequest addDeductionMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddDeductionMasterRequest> apiRequest = getApiRequest(addDeductionMasterRequest, httpServletRequest);
        DeductionMasterResponse deductionMasterResponse = deductionMasterService.addDeduction(apiRequest);
        finalizeResponse(deductionMasterResponse, httpServletRequest);
        return deductionMasterResponse;
    }

    @RequestMapping(value = URI.UPDATE_DEDUCTION_MASTER_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.UPDATE_DEDUCTION_API_NAME, notes = SwaggerMessage.UPDATE_DEDUCTION_API_DESCRIPTION, response = DeductionMasterResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_DEDUCTION_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    DeductionMasterResponse updateDeduction(@PathVariable Long deductionId, @RequestBody AddDeductionMasterRequest addDeductionMasterRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddDeductionMasterRequest> apiRequest = getApiRequest(addDeductionMasterRequest, httpServletRequest);
        apiRequest.setResourceId(deductionId);
        DeductionMasterResponse deductionMasterResponse = deductionMasterService.updateDeduction(apiRequest);
        finalizeResponse(deductionMasterResponse, httpServletRequest);
        return deductionMasterResponse;
    }

    @RequestMapping(value = URI.LIST_DEDUCTION_MASTER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_DEDUCTION_API_NAME, notes = SwaggerMessage.LIST_DEDUCTION_API_DESCRIPTION, response = DeductionMasterListResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    DeductionMasterListResponse getListOfDeductions(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        DeductionMasterListResponse deductionMasterListResponse = deductionMasterService.getListOfDeductions(apiRequest);
        finalizeResponse(deductionMasterListResponse, httpServletRequest);
        return deductionMasterListResponse;
    }


    @RequestMapping(value = URI.DELETE_DEDUCTION_MASTER_URI, method = RequestMethod.DELETE)
    @ApiOperation(value = SwaggerMessage.DELETE_DEDUCTION_API_NAME, notes = SwaggerMessage.DELETE_DEDUCTION_API_DESCRIPTION, response = Response.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_DEDUCTION_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    Response deleteDeduction(@PathVariable(name = URI.PATH_VARIABLE_DEDUCTION_ID) Long deductionId, HttpServletRequest httpServletRequest) throws Exception {
        Response response = deductionMasterService.deleteDeduction(deductionId);
        finalizeResponse(response, httpServletRequest);
        return response;
    }


}
