package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.DeviceMasterDto;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public class DeviceMasterResponse extends Response {
    private DeviceMasterDto deviceDetails;

    public DeviceMasterDto getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(DeviceMasterDto deviceDetails) {
        this.deviceDetails = deviceDetails;
    }
}
