package com.digitalconsultant.api.pojo.request;

import com.digitalconsultant.datamodel.enumeration.Shift;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */

@ApiModel("Login Request")
public class LoginRequest extends Request {

    @ApiModelProperty(value = "userName", required = true, position = 0)
    private String userName;
    @ApiModelProperty(value = "password", required = true, position = 1)
    private String password;
    @ApiModelProperty(value = "shift", required = true, position = 2)
    private Shift shift;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }
}
