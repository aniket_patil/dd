package com.digitalconsultant.api.controller;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.SwaggerMessage;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddCustomerAccountRequest;
import com.digitalconsultant.api.pojo.request.AddCustomerRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.CustomerAccountResponse;
import com.digitalconsultant.api.pojo.response.CustomerListResponse;
import com.digitalconsultant.api.pojo.response.CustomerResponse;
import com.digitalconsultant.api.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
@Controller
@Api(value = "Customer API", description = SwaggerMessage.CUSTOMER_CONTROLLER_DESCRIPTION, position = 0)
public class CustomerController extends AbstractController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = URI.ADD_CUSTOMER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_CUSTOMER_API_NAME, notes = SwaggerMessage.ADD_CUSTOMER_API_DESCRIPTION, response = CustomerResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    CustomerResponse addCustomer(@RequestBody AddCustomerRequest addCustomerRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddCustomerRequest> apiRequest = getApiRequest(addCustomerRequest, httpServletRequest);
        CustomerResponse customerResponse = customerService.addCustomer(apiRequest);
        finalizeResponse(customerResponse, httpServletRequest);
        return customerResponse;
    }


    @RequestMapping(value = URI.UPDATE_CUSTOMER_URI, method = RequestMethod.PUT)
    @ApiOperation(value = SwaggerMessage.UPDATE_CUSTOMER_API_NAME, notes = SwaggerMessage.UPDATE_CUSTOMER_API_DESCRIPTION, response = CustomerResponse.class, position = 1)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header"),
            @ApiImplicitParam(name = URI.PATH_VARIABLE_CUSTOMER_ID, dataType = "String", paramType = "path", required = true)
    })
    public
    @ResponseBody
    CustomerResponse updateCustomer(@PathVariable(name = URI.PATH_VARIABLE_CUSTOMER_ID) Long customerId, @RequestBody AddCustomerRequest addCustomerRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddCustomerRequest> apiRequest = getApiRequest(addCustomerRequest, httpServletRequest);
        apiRequest.setResourceId(customerId);
        CustomerResponse customerResponse = customerService.updateCustomer(apiRequest);
        finalizeResponse(customerResponse, httpServletRequest);
        return customerResponse;
    }


    @RequestMapping(value = URI.LIST_CUSTOMER_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.LIST_CUSTOMER_API_NAME, notes = SwaggerMessage.LIST_CUSTOMERS_API_DESCRIPTION, response = CustomerResponse.class, position = 2)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    CustomerListResponse getListOfCustomers(@RequestBody ResourceListRequest resourceListRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<ResourceListRequest> apiRequest = getApiRequest(resourceListRequest, httpServletRequest);
        CustomerListResponse customerListResponse = customerService.getListOfCustomers(apiRequest);
        finalizeResponse(customerListResponse, httpServletRequest);
        return customerListResponse;
    }

    @RequestMapping(value = URI.ADD_CUSTOMER_ACCOUNT_URI, method = RequestMethod.POST)
    @ApiOperation(value = SwaggerMessage.ADD_CUSTOMER_ACCOUNT_API_NAME, notes = SwaggerMessage.ADD_CUSTOMER_ACCOUNT_API_DESCRIPTION, response = CustomerAccountResponse.class, position = 0)
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constants.API_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.DEVICE_ID_SESSION_KEY_NAME, dataType = "String", paramType = "header", required = true),
            @ApiImplicitParam(name = Constants.LANGUAGE_KEY_NAME, dataType = "String", paramType = "header")
    })
    public
    @ResponseBody
    CustomerAccountResponse addCustomerAccount(@RequestBody AddCustomerAccountRequest addCustomerAccountRequest, HttpServletRequest httpServletRequest) throws Exception {
        ApiRequestDto<AddCustomerAccountRequest> apiRequest = getApiRequest(addCustomerAccountRequest, httpServletRequest);
        CustomerAccountResponse customerAccountResponse = customerService.addAccount(apiRequest);
        finalizeResponse(customerAccountResponse, httpServletRequest);
        return customerAccountResponse;
    }
}
