package com.digitalconsultant.api.pojo.response;

import com.digitalconsultant.api.dto.CustomerDto;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
public class CustomerResponse extends Response {
    private CustomerDto customerDetails;

    public CustomerDto getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDto customerDetails) {
        this.customerDetails = customerDetails;
    }
}
