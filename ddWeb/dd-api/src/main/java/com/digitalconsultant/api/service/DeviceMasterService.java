package com.digitalconsultant.api.service;

import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.AddDeviceMasterRequest;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.response.DeviceMasterListResponse;
import com.digitalconsultant.api.pojo.response.DeviceMasterResponse;
import com.digitalconsultant.api.pojo.response.Response;

/**
 * Created by aniket.patil_3887 on 2/16/2017.
 */
public interface DeviceMasterService {

    DeviceMasterResponse addDevice(ApiRequestDto<AddDeviceMasterRequest> apiRequestDto);

    DeviceMasterResponse updateDevice(ApiRequestDto<AddDeviceMasterRequest> apiRequestDto) throws Exception;

    DeviceMasterListResponse getListOfDevices(ApiRequestDto<ResourceListRequest> apiRequestDto);

    Response deleteDevice(Long deviceId) throws Exception;
}
