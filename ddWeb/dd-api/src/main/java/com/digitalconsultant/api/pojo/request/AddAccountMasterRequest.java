package com.digitalconsultant.api.pojo.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@ApiModel("Account Master Request")
public class AddAccountMasterRequest extends Request {
    @ApiModelProperty(name = "accountName", required = true, position = 0)
    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
