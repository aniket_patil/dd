package com.digitalconsultant.api.service.impl;

import com.digitalconsultant.api.dto.AccountMasterDto;
import com.digitalconsultant.api.dto.ApiRequestDto;
import com.digitalconsultant.api.pojo.request.ResourceListRequest;
import com.digitalconsultant.api.pojo.request.AddAccountMasterRequest;
import com.digitalconsultant.api.pojo.response.AccountMasterListResponse;
import com.digitalconsultant.api.pojo.response.AccountMasterResponse;
import com.digitalconsultant.api.service.AccountMasterService;
import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.AccountEntity;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import com.digitalconsultant.datamodel.enumeration.AccountIdentifier;
import com.digitalconsultant.persistence.dao.AccountEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Service
public class AccountMasterServiceImpl implements AccountMasterService {

    @Autowired
    private AccountEntityDao accountEntityDao;

    @Override
    public AccountMasterResponse addAccount(ApiRequestDto<AddAccountMasterRequest> apiRequestDto) {

        AddAccountMasterRequest addAccountMasterRequest = apiRequestDto.getRequestData();
        DDSessionEntity ddSessionEntity = apiRequestDto.getDdSessionEntity();

        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccount(addAccountMasterRequest.getAccountName());
        accountEntity.setAccountIdentifier(AccountIdentifier.NONE);
        accountEntity.setDairyEntity(ddSessionEntity.getDairyEntity());
        accountEntityDao.insertEntity(accountEntity);

        AccountMasterResponse accountMasterResponse = new AccountMasterResponse();
        accountMasterResponse.setMessage("message.success.accountMaster.add");
        AccountMasterDto accountMasterDto = getAccountMasterDto(accountEntity);
        accountMasterResponse.setAccountDetails(accountMasterDto);

        return accountMasterResponse;
    }


    @Override
    public AccountMasterResponse updateAccount(ApiRequestDto<AddAccountMasterRequest> apiRequestDto) {
        AddAccountMasterRequest addAccountMasterRequest = apiRequestDto.getRequestData();
        Long accountId = (Long) apiRequestDto.getResourceId();

        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setId(accountId);
        accountEntity.setAccount(addAccountMasterRequest.getAccountName());
        accountEntityDao.updateEntity(accountEntity);

        AccountMasterResponse accountMasterResponse = new AccountMasterResponse();
        AccountMasterDto accountMasterDto = getAccountMasterDto(accountEntity);
        accountMasterResponse.setAccountDetails(accountMasterDto);
        accountMasterResponse.setMessage("message.success.accountMaster.update");

        return accountMasterResponse;
    }

    @Override
    public AccountMasterListResponse getListOfAccounts(ApiRequestDto<ResourceListRequest> apiRequestDto) {

        DDSessionEntity ddSessionEntity = apiRequestDto.getDdSessionEntity();
        ResourceListRequest resourceListRequest = apiRequestDto.getRequestData();

        DatabaseCriteria<AccountEntity> databaseCriteria = new DatabaseCriteria<>(AccountEntity.class);
        databaseCriteria.setLimit(resourceListRequest.getLimit());
        databaseCriteria.setOffset(resourceListRequest.getOffset());
        Long dairyId = ddSessionEntity.getDairyEntity().getId();
        List<AccountEntity> accountEntityList = accountEntityDao.findAccountsByDairyId(dairyId, databaseCriteria);

        AccountMasterListResponse accountMasterListResponse = new AccountMasterListResponse();
        if (!ValidationUtils.isListNull(accountEntityList)) {
            List<AccountMasterDto> accountList = new ArrayList<>(accountEntityList.size());
            for (Iterator<AccountEntity> iterator = accountEntityList.iterator(); iterator.hasNext(); ) {
                AccountEntity accountEntity = iterator.next();
                AccountMasterDto accountMasterDto = getAccountMasterDto(accountEntity);
                accountList.add(accountMasterDto);
            }
            accountMasterListResponse.setAccounts(accountList);
        }
        accountMasterListResponse.setMessage("message.request.processed.success");

        return accountMasterListResponse;
    }


    private AccountMasterDto getAccountMasterDto(AccountEntity accountEntity) {
        AccountMasterDto accountMasterDto = new AccountMasterDto();
        accountMasterDto.setAccountName(accountEntity.getAccount());
        accountMasterDto.setAccountId(accountEntity.getId());
        return accountMasterDto;
    }
}
