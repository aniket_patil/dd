package com.digitalconsultant.api.dto;

import com.digitalconsultant.datamodel.entity.DDSessionEntity;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public class ApiRequestDto<T> {
    private T requestData;
    private DDSessionEntity ddSessionEntity;
    private String deviceId;
    private Object resourceId;

    public T getRequestData() {
        return requestData;
    }

    public void setRequestData(T requestData) {
        this.requestData = requestData;
    }

    public DDSessionEntity getDdSessionEntity() {
        return ddSessionEntity;
    }

    public void setDdSessionEntity(DDSessionEntity ddSessionEntity) {
        this.ddSessionEntity = ddSessionEntity;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Object getResourceId() {
        return resourceId;
    }

    public void setResourceId(Object resourceId) {
        this.resourceId = resourceId;
    }

    public Long getDairyId() {
        return ddSessionEntity.getDairyEntity().getId();
    }
}
