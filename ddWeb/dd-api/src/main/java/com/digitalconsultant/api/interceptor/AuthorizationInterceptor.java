package com.digitalconsultant.api.interceptor;

import com.digitalconsultant.api.data.Constants;
import com.digitalconsultant.api.data.URI;
import com.digitalconsultant.api.exception.CustomException;
import com.digitalconsultant.api.service.SessionService;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private SessionService sessionService;
    private List<String> bypassUrlList;

    public void setBypassUrlList(List<String> bypassUrlList) {
        this.bypassUrlList = bypassUrlList;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String sessionToken = request.getHeader(Constants.API_KEY_NAME);
        String deviceId = request.getHeader(Constants.DEVICE_ID_SESSION_KEY_NAME);
        boolean processRequest = false;
        DDSessionEntity ddSessionEntity = null;
        String servletPath = request.getServletPath();
        if (servletPath.contains(URI.PATH_INFO)) {
            if (!ValidationUtils.isStringNull(sessionToken)) {
                ddSessionEntity = sessionService.getSessionDetails(sessionToken, deviceId);
            }

            if (ddSessionEntity != null) {
                processRequest = true;
                request.setAttribute(Constants.SESSION_KEY_NAME, ddSessionEntity);
            } else if (bypassUrlList.contains(servletPath)) {
                processRequest = true;
            } else {
                throw new CustomException("error.sessionToken.invalid");
            }
        } else {
            processRequest = true;
        }


        return processRequest;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        super.postHandle(request, response, handler, modelAndView);
    }
}
