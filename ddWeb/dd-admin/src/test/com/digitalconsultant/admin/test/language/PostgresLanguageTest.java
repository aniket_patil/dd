package com.digitalconsultant.admin.test.language;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.*;
import java.util.UUID;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
public class PostgresLanguageTest {

    public static void main(String... strings) throws ClassNotFoundException, SQLException {
        System.out.println("Hello");
        Class.forName("org.postgresql.Driver");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(10);
        String password = bCryptPasswordEncoder.encode("admin");
        System.out.println(password);


        UUID uuid=UUID.randomUUID();
        System.out.println(uuid.toString().replace("-","").length());
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/digital_dairy?useUnicode=yes&characterEncoding=UTF-8", "postgres", "");
        System.out.println(connection);


//        PreparedStatement insertStmt = connection.prepareStatement("INSERT INTO public.t_mst_account(account, identifier, fk_dairy) VALUES ( ?, ?, ?)");
//        insertStmt.setString(1, "म्हेस अडवान्स");
//        insertStmt.setString(2, "");
//        insertStmt.setLong(3, 1);
//        insertStmt.executeUpdate();
//        insertStmt.close();


        PreparedStatement preparedStatement = connection.prepareStatement("select account from t_mst_account");
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String value = resultSet.getString(1);
            System.out.println(value);
        }
        resultSet.close();
        preparedStatement.close();
        connection.close();


    }
}
