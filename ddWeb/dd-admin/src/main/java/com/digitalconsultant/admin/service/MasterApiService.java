package com.digitalconsultant.admin.service;

import com.digitalconsultant.common.dto.AccountDto;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/11/2017.
 */
public interface MasterApiService {

    List<AccountDto> getListOfAccount(Long dairyId);
}
