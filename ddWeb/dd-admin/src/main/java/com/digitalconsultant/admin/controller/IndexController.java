package com.digitalconsultant.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.AccountEntity;
import com.digitalconsultant.persistence.dao.AccountEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private AccountEntityDao accountEntityDao;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLoginPage(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String view = "login";
        return view;
    }


    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public
    @ResponseBody
    List<AccountEntity> getAccountEntityList(HttpServletResponse httpServletResponse) {
        List<AccountEntity> accountEntities = null;
        DatabaseCriteria<AccountEntity> databaseCriteria = new DatabaseCriteria<>(AccountEntity.class);
        accountEntities = accountEntityDao.findEntitiesByCriteria(databaseCriteria);
        httpServletResponse.setCharacterEncoding(StandardCharsets.UTF_8.name());
        return accountEntities;
    }


}
