package com.digitalconsultant.admin.service.impl;

import java.util.HashSet;
import java.util.Set;

import com.digitalconsultant.persistence.dao.DairyEntityDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private DairyEntityDao dairyEntityDao;

    @Override
    public UserDetails loadUserByUsername(String arg0) throws UsernameNotFoundException {
        Set<GrantedAuthority> authorities = new HashSet<>(1);
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        return new User("admin", "admin", authorities);
    }

}