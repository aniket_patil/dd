package com.digitalconsultant.admin.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
public class MainInterceptor extends HandlerInterceptorAdapter {

    private final Logger logger = LoggerFactory.getLogger(MainInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.info("RequestURI: {}", request.getRequestURI());
        return true;
    }
}
