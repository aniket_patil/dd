package com.digitalconsultant.cloud.service;

/**
 * Created by aniket.patil_3887 on 2/21/2017.
 */
public interface FileService {

    void createFolder(String folderName);

    void deleteFolder(String folderName);

    String createObject(String folderName, String fileName, byte[] fileContents);

    byte[] getObject();

}
