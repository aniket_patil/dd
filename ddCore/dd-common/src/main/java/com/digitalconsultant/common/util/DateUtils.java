package com.digitalconsultant.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtils {

    public static final String TIME_FORMAT = "HH:mm:ss";
    public static final String DATE_FORMAT = "dd/MM/yyyy";

    public static String getCurrentDateWithTime(String dateStr) {

        // DateFormat readFormat = new SimpleDateFormat( "EEE MMM dd yyyy hh:mm
        // aaa");
        DateFormat readFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);

        DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String formattedDate = "";
        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        System.out.println(formattedDate);
        return formattedDate;
    }

    public static String getWorkingDate() {
        String workingDate = null;
        Date date = null;
        SimpleDateFormat dateFormat = null;
        try {
            date = new Date(Calendar.getInstance().getTimeInMillis());
            dateFormat = new SimpleDateFormat(DATE_FORMAT);
            workingDate = dateFormat.format(date);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return workingDate;
    }

    public static Date getCurrentDate() {
        Date currentDate = null;
        try {
            currentDate = new Date(Calendar.getInstance().getTimeInMillis());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return currentDate;
    }

    public static Date getTime(String time) {
        Date date = null;
        try {
            date = new Date(new SimpleDateFormat("HH:mm:ss").parse(time).getTime());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return date;
    }

    public static Date parseDate(String dateInString) {
        Date date = null;
        try {
            if (dateInString != null && dateInString.trim().length() > 0) {
                date = new SimpleDateFormat(DATE_FORMAT).parse(dateInString);
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return date;
    }

    public static Date parseDate(String dateInString, String format) {
        Date date = null;
        try {
            if (dateInString != null && dateInString.trim().length() > 0) {
                date = new SimpleDateFormat(format).parse(dateInString);
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return date;
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        try {

            cal.setTime(date);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cal.getTime();
    }

    public static String formatDate(Date date) {
        String formatedDate = null;
        try {
            if (date != null)
                formatedDate = new SimpleDateFormat(DATE_FORMAT).format(date);
            else
                formatedDate = "";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return formatedDate;
    }

    public static String formatDate(Date date, String pattern) {
        String formatedDate = null;
        try {
            if (date != null)
                formatedDate = new SimpleDateFormat(pattern).format(date);
            else
                formatedDate = "";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return formatedDate;
    }

    public static String formatTime(Date date) {
        String formatedTime = null;
        try {
            if (date != null)
                formatedTime = new SimpleDateFormat(TIME_FORMAT).format(date);
            else
                formatedTime = "";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return formatedTime;
    }

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {

        List<Date> dates = null;
        try {
            dates = new ArrayList<Date>();
            long interval = 1000 * 60 * 60;
            long endTime = enddate.getTime();

            long currentTime = startdate.getTime();

            while (currentTime <= endTime) {
                dates.add(new Date(currentTime));
                currentTime += interval;
            }

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return dates;

    }

    public static LinkedList<Date> searchBetweenDates(Date startDate, Date endDate) throws ParseException {

        LinkedList<Date> list = null;
        LinkedList<Date> hitList = null;
        try {
            hitList = new LinkedList<Date>();
            Date begin = new Date(startDate.getTime());
            list = new LinkedList<Date>();
            list.add(new Date(begin.getTime()));

            while (begin.compareTo(endDate) < 0) {

                begin = new Date(begin.getTime() + 86400000);
                list.add(new Date(begin.getTime()));
            }
            String[] comboDates = new String[hitList.size()];
            for (int i = 0; i < hitList.size(); i++)
                comboDates[i] = new SimpleDateFormat("dd/MM/yyyy").format(((Date) hitList.get(i)));

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return list;
    }

    public static String formatDate(String date, String pattern) {

        String formatedDate = null;
        try {
            if (date != null)
                formatedDate = new SimpleDateFormat(pattern).format(date);
            else
                formatedDate = "";
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return formatedDate;
    }

    public static Date getOnlyDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) date.clone());
        calendar.clear(Calendar.HOUR_OF_DAY);
        calendar.clear(Calendar.AM_PM);
        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        return calendar.getTime();
    }

    public static Date getOnlyTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) date.clone());
        calendar.clear(Calendar.DATE);
        calendar.clear(Calendar.MONTH);
        calendar.clear(Calendar.DAY_OF_MONTH);
        calendar.clear(Calendar.YEAR);
        return calendar.getTime();
    }

    public static Date getDaysBefore(Date date, int dayCount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) date.clone());
        int year = calendar.get(Calendar.YEAR);
        int month = (calendar.get(Calendar.MONTH) + 1);
        int day = calendar.get(Calendar.DATE) - dayCount;

        StringBuffer newDate = new StringBuffer();
        newDate.append(day);
        newDate.append("/");
        newDate.append(month);
        newDate.append("/");
        newDate.append(year);

        Date resultDate = DateUtils.parseDate(newDate.toString(), "dd/MM/yyyy");

        return resultDate;
    }

    public static Date getDaysAfter(Date date, int dayCount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((Date) date.clone());
        int year = calendar.get(Calendar.YEAR);
        int month = (calendar.get(Calendar.MONTH) + 1);
        int day = calendar.get(Calendar.DATE) + dayCount;

        StringBuffer newDate = new StringBuffer();
        newDate.append(day);
        newDate.append("/");
        newDate.append(month);
        newDate.append("/");
        newDate.append(year);

        Date resultDate = DateUtils.parseDate(newDate.toString(), "dd/MM/yyyy");

        return resultDate;
    }

    public static Long getTimeStamp() {
        Long currentTimeStamp = null;
        try {
            currentTimeStamp = Calendar.getInstance().getTimeInMillis();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return currentTimeStamp;
    }

    public static Long getTimeStamp(Date date) {
        Long currentTimeStamp = null;
        try {

            Calendar c = Calendar.getInstance();
            c.setTime(date);
            Long time = c.getTimeInMillis();
            Long curr = System.currentTimeMillis();
            Long diff = curr - time; // Time difference in millisecon
            currentTimeStamp = diff;

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return currentTimeStamp;
    }

    public static Date getDateBefore(int noOfDays, Date dateBefore) {
        Date date = null;
        Calendar calendar = null;
        try {
            calendar = Calendar.getInstance();
            calendar.setTime(dateBefore);
            calendar.add(Calendar.DATE, -noOfDays);
            date = calendar.getTime();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return date;
    }

    public static Date toDate(java.sql.Timestamp timestamp) {
        long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
        return new Date(milliseconds);
    }

    public static boolean isDateAfterCurrentDate(Date date) {
        boolean result = false;
        result = date.after(DateUtils.getCurrentDate());
        return result;
    }

    public static int getNoOfDaysBetweenDates(Date fromDate, Date toDate) {
        int noOfDays = 0;
        try {
            long differenceTime = toDate.getTime() - fromDate.getTime();
            noOfDays = (int) differenceTime / (24 * 60 * 60 * 1000) + 1;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return noOfDays;
    }
}
