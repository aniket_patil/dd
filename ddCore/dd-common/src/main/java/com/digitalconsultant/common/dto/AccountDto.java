package com.digitalconsultant.common.dto;

import java.io.Serializable;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
public class AccountDto implements Serializable {
    private long accountId;
    private String accountName;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
