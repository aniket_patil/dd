package com.digitalconsultant.common.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
public class DatabaseCriteria<T> {

    public enum Order {
        asc, desc
    }

    public class OrderDetails {
        private String columnName;
        private Order order;

        public OrderDetails(String columnName, Order order) {
            this.columnName = columnName;
            this.order = order;
        }

        public String getColumnName() {
            return columnName;
        }

        public Order getOrder() {
            return order;
        }
    }


    public class FetchStrategy {
        private String propertyName;
        private Enum fetchMode;

        public FetchStrategy(String propertyName, Enum fetchMode) {
            this.propertyName = propertyName;
            this.fetchMode = fetchMode;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public Enum getFetchMode() {
            return fetchMode;
        }
    }

    public class AliasDetails {
        private String aliasProperty;
        private String aliasName;

        public AliasDetails(String aliasProperty, String aliasName) {
            this.aliasProperty = aliasProperty;
            this.aliasName = aliasName;
        }

        public String getAliasProperty() {
            return aliasProperty;
        }

        public void setAliasProperty(String aliasProperty) {
            this.aliasProperty = aliasProperty;
        }

        public String getAliasName() {
            return aliasName;
        }

        public void setAliasName(String aliasName) {
            this.aliasName = aliasName;
        }
    }

    private Class<T> criteriaClass;
    private Serializable id;
    private Map<String, Object> andCriteria;
    private List<OrderDetails> orderList;
    private List<AliasDetails> aliasList;
    private List<FetchStrategy> fetchStrategyList;
    private Integer offset;
    private Integer limit;
    private String searchString;


    public DatabaseCriteria(Class<T> criteriaClass) {
        this.criteriaClass = criteriaClass;
    }

    public Class<T> getCriteriaClass() {
        return criteriaClass;
    }

    public void setCriteriaClass(Class<T> criteriaClass) {
        this.criteriaClass = criteriaClass;
    }

    public Serializable getId() {
        return id;
    }

    public void setId(Serializable id) {
        this.id = id;
    }

    public Map<String, Object> getAndCriteria() {
        return andCriteria;
    }


    public List<OrderDetails> getOrderList() {
        return orderList;
    }

    public List<AliasDetails> getAliasList() {
        return aliasList;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public DatabaseCriteria<T> where(String propertyName, Object value) {
        if (andCriteria == null)
            andCriteria = new HashMap<>();
        andCriteria.put(propertyName, value);
        return this;
    }

    public DatabaseCriteria<T> orderBy(String propertyName, Order orderDirect) {
        if (orderList == null)
            orderList = new ArrayList<>();
        OrderDetails orderDetails = new OrderDetails(propertyName, orderDirect);
        orderList.add(orderDetails);
        return this;
    }

    public DatabaseCriteria<T> alias(String aliasProperty, String aliasName) {
        if (aliasList == null)
            aliasList = new ArrayList<>();
        AliasDetails aliasDetails = new AliasDetails(aliasProperty, aliasName);
        aliasList.add(aliasDetails);
        return this;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public DatabaseCriteria<T> fetchStrategy(String propertyName, Enum fetchMode) {
        if (fetchStrategyList == null)
            fetchStrategyList = new ArrayList<>();
        FetchStrategy fetchStrategy = new FetchStrategy(propertyName, fetchMode);
        fetchStrategyList.add(fetchStrategy);
        return this;
    }

    public List<FetchStrategy> getFetchStrategyList() {
        return fetchStrategyList;
    }
}
