package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "state")
@Table(name = "t_mst_state")
public class StateEntity {
    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "state_seq")
    @SequenceGenerator(name = "state_seq", sequenceName = "pk_id_mst_state", allocationSize = 1)
    private Long id;
    @Column(name = "state")
    private String state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
