package com.digitalconsultant.datamodel.entity;

import com.digitalconsultant.datamodel.enumeration.AccountIdentifier;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */

@Entity(name = "account")
@Table(name = "t_mst_account")
public class AccountEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq")
    @SequenceGenerator(name = "account_seq", sequenceName = "pk_id_mst_account", allocationSize = 1)
    private Long id;
    @Column(name = "account")
    private String account;
    @Column(name = "identifier",updatable = false)
    @Enumerated(EnumType.STRING)
    private AccountIdentifier accountIdentifier;
    @ManyToOne
    @JoinColumn(name = "fk_dairy",updatable = false)
    private DairyEntity dairyEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public AccountIdentifier getAccountIdentifier() {
        return accountIdentifier;
    }

    public void setAccountIdentifier(AccountIdentifier accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }
}
