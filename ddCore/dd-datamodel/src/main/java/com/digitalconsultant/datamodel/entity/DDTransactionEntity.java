package com.digitalconsultant.datamodel.entity;

import com.digitalconsultant.datamodel.enumeration.TransactionStatus;
import com.digitalconsultant.datamodel.enumeration.TransactionType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */
@Entity(name = "ddTransaction")
@Table(name = "t_dd_transaction")
public class DDTransactionEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ddTransaction_seq")
    @SequenceGenerator(name = "ddTransaction_seq", sequenceName = "pk_id_dd_transaction", allocationSize = 1)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "fk_customer_account", updatable = false)
    private CustomerAccountEntity customerAccountEntity;
    @ManyToOne
    @JoinColumn(name = "fk_account", updatable = false)
    private AccountEntity accountEntity;
    @ManyToOne
    @JoinColumn(name = "fk_dairy", updatable = false)
    private DairyEntity dairyEntity;
    @Column(name = "amount", updatable = false)
    private Double amount;
    @Column(name = "narration")
    private String narration;
    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionType transactionType;
    @Temporal(TemporalType.DATE)
    @Column(name = "transaction_date", updatable = false)
    private Date transactionDate;
    @Temporal(TemporalType.TIME)
    @Column(name = "transaction_time", updatable = false)
    private Date transactionTime;
    @Column(name = "updated_on")
    private Date updatedOn;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TransactionStatus transactionStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerAccountEntity getCustomerAccountEntity() {
        return customerAccountEntity;
    }

    public void setCustomerAccountEntity(CustomerAccountEntity customerAccountEntity) {
        this.customerAccountEntity = customerAccountEntity;
    }

    public AccountEntity getAccountEntity() {
        return accountEntity;
    }

    public void setAccountEntity(AccountEntity accountEntity) {
        this.accountEntity = accountEntity;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(Date transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public TransactionStatus getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
}
