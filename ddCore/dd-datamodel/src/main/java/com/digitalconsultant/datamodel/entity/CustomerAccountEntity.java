package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "customerAccount")
@Table(name = "t_customer_account")
public class CustomerAccountEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customerAccount_seq")
    @SequenceGenerator(name = "customerAccount_seq", sequenceName = "pk_id_customer_account", allocationSize = 1)
    private Long id;
    @Column(name = "account_id")
    private Long accountNo;
    @ManyToOne
    @JoinColumn(name = "fk_customer")
    private CustomerEntity customerEntity;
    @ManyToOne
    @JoinColumn(name = "fk_milk_type")
    private MilkTypeEntity milkTypeEntity;
    @Embedded
    private BankAccountEntity bankAccountEntity;
    @Column(name = "status")
    private Boolean status;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "added_on")
    private Date addedOn;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }

    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    public MilkTypeEntity getMilkTypeEntity() {
        return milkTypeEntity;
    }

    public void setMilkTypeEntity(MilkTypeEntity milkTypeEntity) {
        this.milkTypeEntity = milkTypeEntity;
    }

    public BankAccountEntity getBankAccountEntity() {
        return bankAccountEntity;
    }

    public void setBankAccountEntity(BankAccountEntity bankAccountEntity) {
        this.bankAccountEntity = bankAccountEntity;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
