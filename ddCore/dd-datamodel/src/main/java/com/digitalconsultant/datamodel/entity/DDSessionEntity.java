package com.digitalconsultant.datamodel.entity;

import com.digitalconsultant.datamodel.enumeration.Shift;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/12/2017.
 */
@Entity(name = "sessionEntity")
@Table(name = "t_dd_session")
@NamedQueries({
        @NamedQuery(name = "getSessionDetailsBySessionToken", query = "select ds from sessionEntity as ds where ds.sessionToken=:sessionToken and ds.deviceId=:deviceId")
})
public class DDSessionEntity {

    @Id
    @Column(name = "session_token")
    private String sessionToken;
    @Column(name = "fk_device")
    private String deviceId;
    @ManyToOne
    @JoinColumn(name = "fk_dairy")
    private DairyEntity dairyEntity;
    @Column(name = "user_name")
    private String userName;
    @Enumerated(EnumType.STRING)
    @Column(name = "shift")
    private Shift shift;

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }
}
