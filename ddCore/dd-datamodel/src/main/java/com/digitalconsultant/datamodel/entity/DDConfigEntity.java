package com.digitalconsultant.datamodel.entity;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by aniket.patil_3887 on 2/13/2017.
 */
@Entity(name = "ddConfig")
@Table(name = "t_dd_config")
public class DDConfigEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ddConfig_seq")
    @SequenceGenerator(name = "ddConfig_seq", sequenceName = "pk_id_dd_config", allocationSize = 1)
    private Long id;
    @Temporal(TemporalType.DATE)
    @Column(name = "bill_date")
    private Date billDate;
    @Column(name = "bill_span")
    private Integer billSpan;
    @ManyToOne
    @JoinColumn(name = "fk_dairy")
    private DairyEntity dairyEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public Integer getBillSpan() {
        return billSpan;
    }

    public void setBillSpan(Integer billSpan) {
        this.billSpan = billSpan;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }
}
