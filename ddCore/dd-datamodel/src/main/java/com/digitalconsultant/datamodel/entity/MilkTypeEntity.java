package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "milkType")
@Table(name = "t_mst_milk_type")
public class MilkTypeEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "milkType_seq")
    @SequenceGenerator(name = "milkType_seq", sequenceName = "pk_id_mst_district", allocationSize = 1)
    private Long id;
    @Column(name = "milk_type")
    private String milkType;
    @ManyToOne
    @JoinColumn(name = "fk_dairy",updatable = false)
    private DairyEntity dairyEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMilkType() {
        return milkType;
    }

    public void setMilkType(String milkType) {
        this.milkType = milkType;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }
}
