package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "customer")
@Table(name = "t_customer_dtls")
public class CustomerEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    @SequenceGenerator(name = "customer_seq", sequenceName = "pk_id_customer_dtls", allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String customerName;
    @Column(name = "mobile_no")
    private String mobileNo;
    @Column(name = "adhar_no")
    private String adharNo;
    @Column(name = "photo_id",updatable = false)
    private String photoId;
    @ManyToOne
    @JoinColumn(name = "fk_dairy",updatable = false)
    private DairyEntity dairyEntity;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "added_on",updatable = false)
    private Date addedOn;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_on")
    private Date updatedOn;
    @Column(name = "status",updatable = false)
    private Boolean status;
    @OneToMany(mappedBy = "customerEntity")
    private List<CustomerAccountEntity> customerAccountEntities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAdharNo() {
        return adharNo;
    }

    public void setAdharNo(String adharNo) {
        this.adharNo = adharNo;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<CustomerAccountEntity> getCustomerAccountEntities() {
        return customerAccountEntities;
    }

    public void setCustomerAccountEntities(List<CustomerAccountEntity> customerAccountEntities) {
        this.customerAccountEntities = customerAccountEntities;
    }
}
