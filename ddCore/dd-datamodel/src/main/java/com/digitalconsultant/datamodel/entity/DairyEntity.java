package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "dairy")
@Table(name = "t_dairy_dtls")
public class DairyEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dairy_seq")
    @SequenceGenerator(name = "dairy_seq", sequenceName = "pk_id_dairy_dtls", allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "fk_district")
    private DistrictEntity districtEntity;
    @Column(name = "mobile_no")
    private String mobileNo;
    @Column(name = "email_id")
    private String emailId;
    @Column(name = "status")
    private Boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DistrictEntity getDistrictEntity() {
        return districtEntity;
    }

    public void setDistrictEntity(DistrictEntity districtEntity) {
        this.districtEntity = districtEntity;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
