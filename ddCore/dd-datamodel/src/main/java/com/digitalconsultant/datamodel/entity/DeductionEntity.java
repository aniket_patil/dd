package com.digitalconsultant.datamodel.entity;

import com.digitalconsultant.datamodel.enumeration.DeductionCycle;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */
@Entity(name = "deduction")
@Table(name = "t_mst_deduction")
public class DeductionEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deduction_seq")
    @SequenceGenerator(name = "deduction_seq", sequenceName = "pk_id_mst_deduction", allocationSize = 1)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "fk_account",updatable = false)
    private AccountEntity accountEntity;
    @Column(name = "deduction_amount")
    private Double deductionAmount;
    @Enumerated(EnumType.STRING)
    @Column(name = "deduction_cycle")
    private DeductionCycle deductionCycle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountEntity getAccountEntity() {
        return accountEntity;
    }

    public void setAccountEntity(AccountEntity accountEntity) {
        this.accountEntity = accountEntity;
    }

    public Double getDeductionAmount() {
        return deductionAmount;
    }

    public void setDeductionAmount(Double deductionAmount) {
        this.deductionAmount = deductionAmount;
    }

    public DeductionCycle getDeductionCycle() {
        return deductionCycle;
    }

    public void setDeductionCycle(DeductionCycle deductionCycle) {
        this.deductionCycle = deductionCycle;
    }
}
