package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/11/2017.
 */
@Entity(name = "device")
@Table(name = "t_mst_device")
public class DeviceEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_seq")
    @SequenceGenerator(name = "device_seq", sequenceName = "pk_id_mst_device", allocationSize = 1)
    private Long id;
    @Column(name = "device_id")
    private String deviceId;
    @ManyToOne
    @JoinColumn(name = "fk_dairy", updatable = false)
    private DairyEntity dairyEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }

}
