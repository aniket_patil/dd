package com.digitalconsultant.datamodel.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Embeddable
public class BankAccountEntity {

    @Column(name = "bank_account_no")
    private String accountNo;
    @Column(name = "bank_name")
    private String bankName;
    @Column(name = "branch_name")
    private String branchName;
    @Column(name = "ifsc_code")
    private String ifscCode;

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }
}
