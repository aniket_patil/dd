package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "accountSavingDtls")
@Table(name = "t_account_saving_dtls")
public class AccountSavingEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accountSavingDtls_seq")
    @SequenceGenerator(name = "accountSavingDtls_seq", sequenceName = "pk_id_account_savings_dtls", allocationSize = 1)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "fk_account")
    private CustomerAccountEntity customerAccountEntity;
    @Column(name = "saving_amount")
    private Double savingAmount;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "added_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date addedOn;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerAccountEntity getCustomerAccountEntity() {
        return customerAccountEntity;
    }

    public void setCustomerAccountEntity(CustomerAccountEntity customerAccountEntity) {
        this.customerAccountEntity = customerAccountEntity;
    }

    public Double getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(Double savingAmount) {
        this.savingAmount = savingAmount;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
