package com.digitalconsultant.datamodel.entity;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/8/2017.
 */
@Entity(name = "district")
@Table(name = "t_mst_district")
public class DistrictEntity {

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "district_seq")
    @SequenceGenerator(name = "district_seq", sequenceName = "pk_id_mst_district", allocationSize = 1)
    private Long id;
    @Column(name = "district")
    private String district;
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "fk_state_id")
    private StateEntity stateEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public StateEntity getStateEntity() {
        return stateEntity;
    }

    public void setStateEntity(StateEntity stateEntity) {
        this.stateEntity = stateEntity;
    }
}
