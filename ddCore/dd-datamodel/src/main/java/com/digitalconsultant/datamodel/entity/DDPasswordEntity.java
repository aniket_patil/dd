package com.digitalconsultant.datamodel.entity;

import com.digitalconsultant.datamodel.enumeration.PasswordStatus;
import com.digitalconsultant.datamodel.enumeration.UserRole;

import javax.persistence.*;

/**
 * Created by aniket.patil_3887 on 2/11/2017.
 */
@Entity(name = "passwordEntity")
@Table(name = "t_dd_password")
@NamedQueries({
        @NamedQuery(name = "getUserCredentialsByUserName", query = "select ddp from passwordEntity as ddp where ddp.userName=:userName"),
        @NamedQuery(name = "passwordEntity.updatePassword", query = "update passwordEntity set password=:password where userName=:userName")
})
public class DDPasswordEntity {
    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ddPassword_seq")
    @SequenceGenerator(name = "ddPassword_seq", sequenceName = "pk_id_dd_password", allocationSize = 1)
    private Long id;
    @Column(name = "user_name", updatable = false)
    private String userName;
    @Column(name = "password")
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(name = "user_role", updatable = false)
    private UserRole userRole;
    @ManyToOne
    @JoinColumn(name = "fk_dairy", updatable = false)
    private DairyEntity dairyEntity;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PasswordStatus passwordStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public DairyEntity getDairyEntity() {
        return dairyEntity;
    }

    public void setDairyEntity(DairyEntity dairyEntity) {
        this.dairyEntity = dairyEntity;
    }

    public PasswordStatus getPasswordStatus() {
        return passwordStatus;
    }

    public void setPasswordStatus(PasswordStatus passwordStatus) {
        this.passwordStatus = passwordStatus;
    }
}
