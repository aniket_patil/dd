package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.datamodel.entity.DairyEntity;
import com.digitalconsultant.persistence.dao.DairyEntityDao;
import org.springframework.stereotype.Repository;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */

@Repository("dairyEntityDao")
public class DairyEntityDaoImpl extends DDGenericDaoImpl<DairyEntity> implements DairyEntityDao {
}
