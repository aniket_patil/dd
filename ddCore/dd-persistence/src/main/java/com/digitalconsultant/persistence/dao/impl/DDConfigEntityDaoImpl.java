package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.datamodel.entity.DDConfigEntity;
import com.digitalconsultant.persistence.dao.DDConfigEntityDao;
import org.springframework.stereotype.Repository;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Repository
public class DDConfigEntityDaoImpl extends DDGenericDaoImpl<DDConfigEntity> implements DDConfigEntityDao {
}
