package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.CustomerEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
public interface CustomerEntityDao extends DDGenericDao<CustomerEntity> {
    List<CustomerEntity> findCustomersByDairyId(Long dairyId, DatabaseCriteria<CustomerEntity> databaseCriteria);

    CustomerEntity findCustomerEntityByCriteria(DatabaseCriteria<CustomerEntity> databaseCriteria);
}
