package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.CustomerEntity;
import com.digitalconsultant.persistence.dao.CustomerEntityDao;
import org.hibernate.FetchMode;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/19/2017.
 */
@Repository
public class CustomerEntityDaoImpl extends DDGenericDaoImpl<CustomerEntity> implements CustomerEntityDao {
    @Override
    public List<CustomerEntity> findCustomersByDairyId(Long dairyId, DatabaseCriteria<CustomerEntity> databaseCriteria) {
        databaseCriteria.where("dairyEntity.id", dairyId);
        databaseCriteria.fetchStrategy("customerAccountEntities", FetchMode.JOIN);
        List<CustomerEntity> customerEntityList = findEntitiesByCriteria(databaseCriteria);
        return customerEntityList;
    }

    @Override
    public CustomerEntity findCustomerEntityByCriteria(DatabaseCriteria<CustomerEntity> databaseCriteria) {
        databaseCriteria.where("id",databaseCriteria.getId());
        databaseCriteria.fetchStrategy("customerAccountEntities", FetchMode.JOIN);
        CustomerEntity customerEntity=findEntityByCriteria(databaseCriteria);
        return customerEntity;
    }
}
