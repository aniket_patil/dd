package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.datamodel.entity.DDSessionEntity;
import com.digitalconsultant.persistence.dao.DDSessionEntityDao;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Repository("DDSessionEntityDaoImpl")
public class DDSessionEntityDaoImpl extends DDGenericDaoImpl<DDSessionEntity> implements DDSessionEntityDao {


    @Override
    public DDSessionEntity getSessionDetailsBySessionToken(String sessionToken, String deviceId) {
        Map<String, Object> parameterMap = new HashMap(2);
        parameterMap.put("sessionToken", sessionToken);
        parameterMap.put("deviceId", deviceId);
        DDSessionEntity ddSessionEntity = executeNamedQuery("getSessionDetailsBySessionToken", parameterMap);
        return ddSessionEntity;
    }
}
