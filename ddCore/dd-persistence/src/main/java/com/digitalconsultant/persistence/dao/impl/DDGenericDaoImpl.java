package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.common.util.ValidationUtils;
import com.digitalconsultant.persistence.dao.DDGenericDao;
import org.hibernate.*;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */
public class DDGenericDaoImpl<T> implements DDGenericDao<T> {

    private Logger logger = LoggerFactory.getLogger(DDGenericDaoImpl.class);

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession() {
        Session session = sessionFactory.openSession();
        return session;
    }

    @Override
    public T getEntity(DatabaseCriteria<T> databaseCriteria) {
        Session session = null;
        T entity = null;
        try {
            session = getSession();
            entity = (T) session.get(databaseCriteria.getCriteriaClass(), databaseCriteria.getId());
        } finally {
            session.close();
        }
        return entity;
    }

    @Override
    public Serializable insertEntity(T t) {
        Session session = getSession();
        Serializable id = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            id = session.save(t);
            transaction.commit();
        } finally {
            session.close();
        }

        return id;
    }

    @Override
    public boolean updateEntity(T t) {
        boolean result = false;
        Session session = getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(t);
            transaction.commit();
            result = true;
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public boolean deleteEntity(T t) {
        boolean result = false;
        Session session = getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(t);
            transaction.commit();
            result = true;
        } finally {
            session.close();
        }
        return result;
    }

    @Override
    public List<Serializable> bulkInsert(List<T> entityList) {
        Session session = getSession();
        Serializable id = null;
        Transaction transaction = null;
        T t = null;
        List<Serializable> listOfIds = null;
        try {
            listOfIds = new ArrayList<>(entityList.size());
            transaction = session.beginTransaction();
            for (Iterator<T> iterator = entityList.iterator(); iterator.hasNext(); ) {
                t = iterator.next();
                id = session.save(t);
                listOfIds.add(id);
            }
            transaction.commit();
        } finally {
            session.close();
        }
        return listOfIds;
    }

    @Override
    public List<T> findEntitiesByCriteria(DatabaseCriteria<T> databaseCriteria) {
        List<T> list = null;
        Session session = null;
        try {
            session = getSession();
            Criteria criteria = getCriteria(databaseCriteria, session);
            if (databaseCriteria.getOffset() != null) {
                criteria.setFirstResult(databaseCriteria.getOffset());
                criteria.setMaxResults(databaseCriteria.getLimit());
            }
            list = criteria.list();
        } finally {
            session.close();
        }

        return list;
    }

    @Override
    public T findEntityByCriteria(DatabaseCriteria<T> databaseCriteria) {
        Session session = null;
        T entity = null;
        try {
            session = getSession();
            Criteria criteria = getCriteria(databaseCriteria, session);
            entity = (T) criteria.uniqueResult();
        } finally {
            session.close();
        }
        return entity;
    }


    @Override
    public T executeNamedQuery(String queryName, Map<String, Object> parameterMap) {
        Session session = getSession();
        T entity;
        try {
            Query query = getQuery(queryName, parameterMap, session);
            entity = (T) query.uniqueResult();

        } finally {
            session.close();
        }

        return entity;
    }


    @Override
    public boolean executeUpdateNamedQuery(String queryName, Map<String, Object> parameterMap) {
        Session session = getSession();
        boolean result = false;
        try {
            Query query = getQuery(queryName, parameterMap, session);
            int noOfRowsAffected = query.executeUpdate();
            if (noOfRowsAffected > 0) {
                result = true;
            }
        } finally {
            session.close();
        }

        return result;
    }

    private Query getQuery(String queryName, Map<String, Object> parameterMap, Session session) {
        Query query = session.getNamedQuery(queryName);
        if (parameterMap != null && !parameterMap.isEmpty()) {
            for (Map.Entry<String, Object> entry : parameterMap.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }

    private Criteria getCriteria(DatabaseCriteria<T> databaseCriteria, Session session) {
        Criteria criteria = session.createCriteria(databaseCriteria.getCriteriaClass());
        List<DatabaseCriteria<T>.AliasDetails> aliasList = databaseCriteria.getAliasList();
        if (!ValidationUtils.isListNull(aliasList)) {
            for (Iterator<DatabaseCriteria<T>.AliasDetails> iterator = aliasList.iterator(); iterator.hasNext(); ) {
                DatabaseCriteria<T>.AliasDetails aliasDetails = iterator.next();
                criteria.createAlias(aliasDetails.getAliasProperty(), aliasDetails.getAliasName());
            }
        }

        Map<String, Object> andCriteria = databaseCriteria.getAndCriteria();
        if (andCriteria != null && !andCriteria.isEmpty()) {
            criteria.add(Restrictions.allEq(andCriteria));
        }
        List<DatabaseCriteria<T>.FetchStrategy> fetchStrategies = databaseCriteria.getFetchStrategyList();

        if (!ValidationUtils.isListNull(fetchStrategies)) {
            for (Iterator<DatabaseCriteria<T>.FetchStrategy> iterator = fetchStrategies.iterator(); iterator.hasNext(); ) {
                DatabaseCriteria<T>.FetchStrategy fetchStrategy = iterator.next();
                criteria.setFetchMode(fetchStrategy.getPropertyName(), (FetchMode) fetchStrategy.getFetchMode());
            }
        }
        List<DatabaseCriteria<T>.OrderDetails> orderDetailsList = databaseCriteria.getOrderList();
        if (!ValidationUtils.isListNull(orderDetailsList)) {
            for (Iterator<DatabaseCriteria<T>.OrderDetails> iterator = orderDetailsList.iterator(); iterator.hasNext(); ) {
                DatabaseCriteria<T>.OrderDetails orderDetails = iterator.next();
                DatabaseCriteria.Order order = orderDetails.getOrder();
                if (order == DatabaseCriteria.Order.asc) {
                    criteria.addOrder(Order.asc(orderDetails.getColumnName()));
                } else if (order == DatabaseCriteria.Order.desc) {
                    criteria.addOrder(Order.desc(orderDetails.getColumnName()));
                }
            }
        }
        return criteria;
    }


}
