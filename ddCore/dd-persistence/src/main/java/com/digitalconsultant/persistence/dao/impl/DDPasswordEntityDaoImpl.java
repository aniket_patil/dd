package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DDPasswordEntity;
import com.digitalconsultant.persistence.dao.DDPasswordEntityDao;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Repository
public class DDPasswordEntityDaoImpl extends DDGenericDaoImpl<DDPasswordEntity> implements DDPasswordEntityDao {
    @Override
    public DDPasswordEntity getUserCredentialsByUserName(String userName) {
        Map<String, Object> parameterMap = new HashMap(1);
        parameterMap.put("userName", userName);
        DDPasswordEntity ddPasswordEntity = executeNamedQuery("getUserCredentialsByUserName", parameterMap);
        return ddPasswordEntity;
    }

    @Override
    public boolean updatePassword(DDPasswordEntity ddPasswordEntity) {
        Map<String, Object> parameterMap = new HashMap(2);
        parameterMap.put("userName", ddPasswordEntity.getUserName());
        parameterMap.put("password", ddPasswordEntity.getPassword());
        boolean result = executeUpdateNamedQuery("passwordEntity.updatePassword", parameterMap);
        return result;
    }

    @Override
    public List<DDPasswordEntity> findDairyUsersByDairyId(Long dairyId, DatabaseCriteria<DDPasswordEntity> databaseCriteria) {
        databaseCriteria.where("dairyEntity.id", dairyId);
        List<DDPasswordEntity> ddPasswordEntityList = findEntitiesByCriteria(databaseCriteria);
        return ddPasswordEntityList;
    }
}
