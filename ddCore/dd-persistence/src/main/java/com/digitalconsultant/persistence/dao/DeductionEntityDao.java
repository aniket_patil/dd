package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DeductionEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
public interface DeductionEntityDao extends DDGenericDao<DeductionEntity> {

    List<DeductionEntity> findDeductionsByDairyId(Long dairyId, DatabaseCriteria<DeductionEntity> databaseCriteria);
}
