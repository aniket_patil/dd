package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DDPasswordEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface DDPasswordEntityDao extends DDGenericDao<DDPasswordEntity> {

    DDPasswordEntity getUserCredentialsByUserName(String userName);

    boolean updatePassword(DDPasswordEntity ddPasswordEntity);

    List<DDPasswordEntity> findDairyUsersByDairyId(Long dairyId, DatabaseCriteria<DDPasswordEntity> databaseCriteria);
}
