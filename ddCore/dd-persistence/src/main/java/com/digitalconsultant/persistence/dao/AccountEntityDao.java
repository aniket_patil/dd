package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.AccountEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
public interface AccountEntityDao extends DDGenericDao<AccountEntity> {

    List<AccountEntity> findAccountsByDairyId(Long dairyId, DatabaseCriteria<AccountEntity> databaseCriteria);

}
