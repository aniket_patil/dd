package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DeductionEntity;
import com.digitalconsultant.persistence.dao.DeductionEntityDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/15/2017.
 */
@Repository
public class DeductionEntityDaoImpl extends DDGenericDaoImpl<DeductionEntity> implements DeductionEntityDao {
    @Override
    public List<DeductionEntity> findDeductionsByDairyId(Long dairyId, DatabaseCriteria<DeductionEntity> databaseCriteria) {
        databaseCriteria.alias("accountEntity.dairyEntity", "dairyEntity");
        databaseCriteria.where("dairyEntity.id", dairyId);
        List<DeductionEntity> deductionEntityList = super.findEntitiesByCriteria(databaseCriteria);
        return deductionEntityList;
    }
}
