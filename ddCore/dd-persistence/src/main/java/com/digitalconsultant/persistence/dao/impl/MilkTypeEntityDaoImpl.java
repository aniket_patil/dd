package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.MilkTypeEntity;
import com.digitalconsultant.persistence.dao.MilkTypeEntityDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
@Repository
public class MilkTypeEntityDaoImpl extends DDGenericDaoImpl<MilkTypeEntity> implements MilkTypeEntityDao {
    @Override
    public List<MilkTypeEntity> findMilkTypesByDairyId(Long dairyId, DatabaseCriteria<MilkTypeEntity> databaseCriteria) {
        databaseCriteria.where("dairyEntity.id", dairyId);
        List<MilkTypeEntity> milkTypeEntities = findEntitiesByCriteria(databaseCriteria);
        return milkTypeEntities;
    }

    @Override
    public MilkTypeEntity getMilkTypeEntity(Long milkTypeId) {
        DatabaseCriteria<MilkTypeEntity> databaseCriteria = new DatabaseCriteria<>(MilkTypeEntity.class);
        databaseCriteria.setId(milkTypeId);
        return getEntity(databaseCriteria);
    }
}
