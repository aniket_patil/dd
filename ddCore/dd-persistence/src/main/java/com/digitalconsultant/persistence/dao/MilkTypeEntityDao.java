package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.MilkTypeEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/17/2017.
 */
public interface MilkTypeEntityDao extends DDGenericDao<MilkTypeEntity> {

    List<MilkTypeEntity> findMilkTypesByDairyId(Long dairyId, DatabaseCriteria<MilkTypeEntity> databaseCriteria);

    MilkTypeEntity getMilkTypeEntity(Long milkTypeId);
}
