package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DeviceEntity;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface DeviceEntityDao extends DDGenericDao<DeviceEntity> {

    List<DeviceEntity> findDevicesByDairyId(Long dairyId, DatabaseCriteria<DeviceEntity> databaseCriteria);

    DeviceEntity findDeviceByDeviceId(String deviceId);
}
