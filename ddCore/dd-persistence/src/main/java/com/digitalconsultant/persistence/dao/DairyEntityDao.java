package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.datamodel.entity.DairyEntity;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */
public interface DairyEntityDao extends DDGenericDao<DairyEntity> {


}
