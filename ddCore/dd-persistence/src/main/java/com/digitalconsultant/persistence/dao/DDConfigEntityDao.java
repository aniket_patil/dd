package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.datamodel.entity.DDConfigEntity;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface DDConfigEntityDao extends DDGenericDao<DDConfigEntity> {
}
