package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.datamodel.entity.DDSessionEntity;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
public interface DDSessionEntityDao extends DDGenericDao<DDSessionEntity> {

    DDSessionEntity getSessionDetailsBySessionToken(String sessionToken,String deviceId);
}
