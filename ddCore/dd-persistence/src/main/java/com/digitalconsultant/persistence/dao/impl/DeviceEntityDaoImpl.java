package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.DeviceEntity;
import com.digitalconsultant.persistence.dao.DeviceEntityDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/14/2017.
 */
@Repository
public class DeviceEntityDaoImpl extends DDGenericDaoImpl<DeviceEntity> implements DeviceEntityDao {

    @Override
    public List<DeviceEntity> findDevicesByDairyId(Long dairyId, DatabaseCriteria<DeviceEntity> databaseCriteria) {
        databaseCriteria.where("dairyEntity.id", dairyId);
        List<DeviceEntity> deviceEntityList = super.findEntitiesByCriteria(databaseCriteria);
        return deviceEntityList;
    }

    @Override
    public DeviceEntity findDeviceByDeviceId(String deviceId) {
        DatabaseCriteria<DeviceEntity> databaseCriteria = new DatabaseCriteria<>(DeviceEntity.class);
        databaseCriteria.where("deviceId", deviceId);
        DeviceEntity deviceEntity = findEntityByCriteria(databaseCriteria);
        return deviceEntity;
    }
}
