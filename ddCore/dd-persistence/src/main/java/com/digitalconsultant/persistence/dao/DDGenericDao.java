package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.common.pojo.DatabaseCriteria;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by aniket.patil_3887 on 2/9/2017.
 */
public interface DDGenericDao<T> {


    T getEntity(DatabaseCriteria<T> databaseCriteria);

    Serializable insertEntity(T t);

    boolean updateEntity(T t);

    boolean deleteEntity(T t);

    List<Serializable> bulkInsert(List<T> entityList);

    List<T> findEntitiesByCriteria(DatabaseCriteria<T> databaseCriteria);

    T findEntityByCriteria(DatabaseCriteria<T> databaseCriteria);

    T executeNamedQuery(String queryName, Map<String, Object> parameterMap);

    boolean executeUpdateNamedQuery(String queryName, Map<String, Object> parameterMap);
}
