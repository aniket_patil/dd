package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.common.pojo.DatabaseCriteria;
import com.digitalconsultant.datamodel.entity.AccountEntity;
import com.digitalconsultant.persistence.dao.AccountEntityDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by aniket.patil_3887 on 2/10/2017.
 */
@Repository("accountEntityDao")
public class AccountEntityDaoImpl extends DDGenericDaoImpl<AccountEntity> implements AccountEntityDao {

    @Override
    public List<AccountEntity> findAccountsByDairyId(Long dairyId, DatabaseCriteria<AccountEntity> databaseCriteria) {
        databaseCriteria.where("dairyEntity.id", dairyId);
        List<AccountEntity> accountEntityList = super.findEntitiesByCriteria(databaseCriteria);
        return accountEntityList;
    }
}
