package com.digitalconsultant.persistence.dao;

import com.digitalconsultant.datamodel.entity.CustomerAccountEntity;

/**
 * Created by aniket.patil_3887 on 2/20/2017.
 */
public interface CustomerAccountEntityDao extends DDGenericDao<CustomerAccountEntity> {
}
