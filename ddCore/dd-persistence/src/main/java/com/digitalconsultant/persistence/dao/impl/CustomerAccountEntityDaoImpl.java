package com.digitalconsultant.persistence.dao.impl;

import com.digitalconsultant.datamodel.entity.CustomerAccountEntity;
import com.digitalconsultant.persistence.dao.CustomerAccountEntityDao;
import org.springframework.stereotype.Repository;

/**
 * Created by aniket.patil_3887 on 2/20/2017.
 */
@Repository
public class CustomerAccountEntityDaoImpl extends DDGenericDaoImpl<CustomerAccountEntity> implements CustomerAccountEntityDao {
}
